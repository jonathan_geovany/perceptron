/*
 * Clase Aplicacion donde se 
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.hv.perceptron;

/**
 *
 * @author Jonathan
 */
public class Aplicacion {
    int nodosK;
    int nodosJ;
    int entradasK;
    int entradasJ;
    char letras[];
    RedNeuronal red;
    double balace = -1;
    double capaI[][];
    double aprender[][];
    public Aplicacion(int _nodosJ,int _entradasK,double _capaI[][],double _aprender[][],char resultados[]){
        
        capaI = _capaI;
        aprender = _aprender;
        nodosJ = _nodosJ;
        entradasK = _entradasK;
        entradasJ = capaI[0].length+1;
        nodosK = aprender.length;
        
        letras = resultados;
        
        red = new RedNeuronal(nodosK, nodosJ, entradasK, entradasJ);
        
        red.crearRed();
        red.inicializarPesos();
        //iteraciones maximas 60 mil 
        int n=60000;
        int i=0;
        int epoca;
        int k=0;
        boolean noIgual=false;
        while(i<capaI.length && k<n){
            epoca = i;
            
            System.out.println("");
            if(i==0){
                System.out.println("********* Epoca "+ epoca + " ***** Iteracion "+k +" *** ");
            }
            for(int c=0;c<entradasJ-1;c++){
                System.out.print(Math.round(capaI[epoca][c])+",");
            }
            System.out.print(" Aprender ");
            for(int c=0;c<nodosK;c++){
                System.out.print(Math.round(aprender[c][epoca])+",");
            }
            System.out.print(" Resultado ");
            red.entradasJ(capaI[epoca]);
            red.activacionJ();
            red.entradasK();
            red.activacionK();
            red.errorCapaK(aprender, epoca);
            for(int c=0;c<nodosK;c++){
                System.out.print(Math.round(red.getActivacionK(c))+",");
            }
            //conversion de binario a decimal
            double decimal=0;
            int t=0;
            for(int c=nodosK-1;c>=0;c--){
                decimal += red.getActivacionK(c)*Math.pow(2, t);
                t++;
            }
            int ind = (int)decimal;
            if(ind>4){
                ind=0;
            }
            
            System.out.print(" Cant "+decimal+ " Letra " + letras[ind]);
            noIgual=false;
            //evaluando si los nodos aprendieron bien
            for(int c=0;c<nodosK;c++){
                if(red.getActivacionK(c)==aprender[c][epoca]){
                    
                }else{
                    noIgual=true;
                }
            }
            if(noIgual){
                i=-1;
                red.pesos_JK();
                red.errorCapaJ();
                red.pesos_IJ();
            }
            i++;
            k++;
        }
    }
    
    public String verificar(double verifica[]){
        red.entradasJ(verifica);
        red.activacionJ();
        red.entradasK();
        red.activacionK();
        //conversion de binario a decimal
        double decimal=0;
        int t=0;
        for(int c=nodosK-1;c>=0;c--){
            decimal += red.getActivacionK(c)*Math.pow(2, t);
            t++;
        }
        int ind = (int)decimal;
        if(ind>4){
            ind=0;
        }
        System.out.println(" Cant "+decimal+ " Letra " + letras[ind]);
        return ""+letras[ind];
    }
    
    
}
