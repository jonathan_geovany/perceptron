/*
 * Clase Sigmoide
 * Utilizada en la funcion de activacion
 * retorna el valor de las suma de pesos ponderados
 */
package net.hv.perceptron;

/**
 *
 * @author Jonathan Geovany Hernandez
 */
public class Sigmoide {
    private double e;
    private double p;
    
    public Sigmoide(){
        e = Math.E;
        p=1;
    }
    double funcion(double suma){
        return (1/( 1 + Math.pow(e, -suma/p)));
    }
    
}
