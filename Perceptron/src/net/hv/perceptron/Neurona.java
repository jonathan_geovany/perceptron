/*
 * Clase neurona
 * Atributos configurables como:
 * La funcion de activacion por defecto Sigmoide
 * Visualizacion de pesos y de errores
 */
package net.hv.perceptron;

/**
 *
 * @author Jonathan Geovany Hernandez Vasquez
 */
public class Neurona { 
    private double activacionNeurona;
    public double[] entradas;
    public double pesos[];
    private double errorNodo;
    private static Sigmoide sigmoide;
    
    public Neurona(int nEntradas){
        entradas = new double[nEntradas];
        pesos = new double[nEntradas];
        sigmoide = new Sigmoide();
        errorNodo = 0;
    }
    public void activacion(){
        double suma=0;
        for(int i=0;i<entradas.length;i++){
            suma += entradas[i]*pesos[i];
        }
        activacionNeurona = sigmoide.funcion(suma);
    }
    
    public void verEntradas(){
        System.out.println("************* Entradas ***************");
        for(int i=0;i<entradas.length;i++){
           System.out.println(entradas[i]);
        }
    }
    
    public void verPesos(){
        System.out.println("************** Pesos *****************");
        for(int i=0;i<pesos.length;i++){
           System.out.println(pesos[i]);
        }
    }
    
    public void verActivacion(){
        System.out.println("************ Activacion **************");
        System.out.println(activacionNeurona);
    }
    
    public double getActivacion(){
        return activacionNeurona;
    }
    
    public double getError(){
        return errorNodo;
    }
    
    public void setError(double error){
        errorNodo=error;
    }
    
    
    
}
