/*
 * Clase RedNeuronal 
 * Utilizacion de dos capas más una capa de entrada
 * n entradas de cada capa configurable
 * capas
 * entrada  - neurona J  -  neurona K  
 *   [ ] 1      
 *   [ ] 2       ( ) 1       
 *   [ ] 3       ( ) 2        { } 1
 *   [ ] 4        .            .
 *    .           .            .
 *    .           .           { } n
 *    .          ( ) n
 *   [ ] n
 */
package net.hv.perceptron;

/**
 *
 * @author Jonathan Geovany Hernandez
 */
public class RedNeuronal {
    //red neuronal para dos capas (+1 de la entrada)
    private int nodosCapaK, entCapaK , nodosCapaJ, entCapaJ;
    private double aprendizaje = 0.25;//factor de aprendizaje
    private Neurona [] neuronaK, neuronaJ;
    private static double balance = 1;
    
    public RedNeuronal(int nodosK,int nodosJ, int entK,int entJ){
        nodosCapaK = nodosK;
        nodosCapaJ = nodosJ;
        entCapaK = entK;
        entCapaJ = entJ;
    }
    
    public void crearRed(){
        neuronaK= new Neurona[nodosCapaK];
        for(int i=0;i<neuronaK.length;i++){
            neuronaK[i] = new Neurona(entCapaK);
        }
        neuronaJ= new Neurona[nodosCapaJ];
        for(int i=0;i<neuronaJ.length;i++){
            neuronaJ[i] = new Neurona(entCapaJ);
        }
    }
    
    public void inicializarPesos(){
        for(int i=0;i<neuronaK.length;i++){
            for(int j=0;j<entCapaK;j++){
                neuronaK[i].pesos[i] = Math.random();
            }
        }
        
        for(int i=0;i<neuronaJ.length;i++){
            for(int j=0;j<entCapaJ;j++){
                neuronaJ[i].pesos[i] = Math.random();
            }
        }
    }
    
    public void entradasJ(double capaI[]){
        for(int i=0;i< nodosCapaJ;i++){
            for(int j=0;j<capaI.length;j++){
                neuronaJ[i].entradas[j]=capaI[j];
            }
        }
    }
    
    public void activacionJ(){
        for(int i=0;i<neuronaJ.length;i++){
            neuronaJ[i].activacion();
        }
    }
    
    
    public void activacionK(){
        for(int i=0;i<neuronaK.length;i++){
            neuronaK[i].activacion();
        }
    }
    
    public void entradasK(){
        int j;
        for(int i=0;i< nodosCapaK;i++){
            for(j=0;j<nodosCapaJ;j++){
                neuronaK[i].entradas[j]=neuronaJ[j].getActivacion();
            }
            neuronaK[i].entradas[j]=balance;
        }
    }
    
    public void errorCapaK(double aprender[][],int epoca){
        double errorl=0;
        for(int i=0;i<nodosCapaK;i++){
            //preparando los errores para la propagacion hacia atras
            errorl=(aprender[i][epoca] - neuronaK[i].getActivacion())*neuronaK[i].getActivacion()*(1-neuronaK[i].getActivacion());
            neuronaK[i].setError(errorl);
        }
    }
    
    
    public void errorCapaJ(){
        double suma,errorJ;
        for(int i=0;i<nodosCapaJ;i++){
            suma=0;
            for(int j=0;j<nodosCapaK;j++){
                suma+=(neuronaK[j].getError()*neuronaK[j].pesos[i]);
            }
            //preparando los errores de las capas para la propagacion hacia atras
            errorJ=neuronaJ[i].getActivacion()*(1-neuronaJ[i].getActivacion())*suma;
            neuronaJ[i].setError(errorJ);
        }
    }
    
    public void pesos_JK(){
        for(int i=0;i<nodosCapaK;i++){
            for(int j=0;j<entCapaK;j++){
                //pesos propagados de la capa K 
                neuronaK[i].pesos[j]+=(aprendizaje * neuronaK[i].getError() * neuronaK[i].entradas[j] );
            }
        }
    }
    
    public void pesos_IJ(){
        for(int i=0;i<nodosCapaJ;i++){
            for(int j=0;j<entCapaJ;j++){
                //pesos propagados de la capa J
                neuronaJ[i].pesos[j]+=(aprendizaje * neuronaJ[i].getError() * neuronaJ[i].entradas[j] );
            }
        }
    }
    
    public double getErrorK(){
        double suma_e=0;
        for(int i=0;i<nodosCapaK;i++){
            suma_e=neuronaK[i].getError();
        }
        return Math.abs(suma_e);
    }
    
    public double getActivacionK(int neuron){
        //si sobre pasa el valor de aprendizaje minimo
        if(neuronaK[neuron].getActivacion()>=0.5){
            return 1.0;
        }else{
            return 0.0;
        }
    }
}
