/*
 * Implementacion del perceptron multicapa con retropropagación
 * CapaI : valores que aprendera la red
 * Aprender : valores binarios que seran su evaluacion de aprendizaje
 * Letras : Salida de lo valores binarios
 * Verificar : prueba la red
 * nodosJ : los nodos de la primera capa
 * entradasK : las entradas de la segunda capa
 */
package perceptron;

import net.hv.perceptron.Aplicacion;
/**
 *
 * @author Jonathan Geovany Hernandez Vasquez
 */
public class Perceptron {
    static double capaI[][]={
        {
        // A
        1,1,1,1,
        1,0,0,1,
        1,1,1,1,
        1,0,0,1,
        1,0,0,1
        },{
        // C
        1,1,1,1,
        1,0,0,0,
        1,0,0,0,
        1,0,0,0,
        1,1,1,1
        },{
        // E
        1,1,1,1,
        1,0,0,0,
        1,1,1,0,
        1,0,0,0,
        1,1,1,1
        },{
        // F
        1,1,1,1,
        1,0,0,0,
        1,1,1,0,
        1,0,0,0,
        1,0,0,0
        },{
        // L
        1,0,0,0,
        1,0,0,0,
        1,0,0,0,
        1,0,0,0,
        1,1,1,1
        }
    };
    static double aprender[][]=
    {
        {0,0,0,0,1},
        {0,0,1,1,0},
        {0,1,0,1,0}
    };
    static char letras[] = {'A','C','E','F','L'};
    static double verificar[] = {
        // Demostracion de una letra con ruido
        1,1,1,1,
        1,1,0,0,
        1,0,0,0,
        1,0,1,0,
        1,1,1,1
        };
    static Aplicacion app; 
    static int nodosJ = 6;
    static int entradasK = 7;
    
    public static void main(String[] args) {
        // Aplicacion de la red
        app = new Aplicacion(nodosJ,entradasK,capaI, aprender,letras);
        ejemplo();
        
    }
    public static  void ejemplo(){
        System.out.println("\nVerificando");
        app.verificar(verificar);
    }
    
}
