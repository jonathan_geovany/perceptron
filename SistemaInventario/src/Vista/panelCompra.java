/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.Operaciones;
import Controlador.SingleUser;
import Controlador.TableCellListener;
import Modelo.*;
import Reportes.rptFacturaCompra;
import Reportes.rptFacturaVenta;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author Jonathan
 */
public class panelCompra extends javax.swing.JPanel {

    /**
     * Creates new form panelVenta
     */
    private Proveedores proveedor;
    private Set detallesCompra;
    private TableCellListener listenerTabla;
    private int filas = 1;
    public panelCompra() {
        initComponents();
        
        detallesCompra = new HashSet(0);
        Action action = new AbstractAction()
        {
            public void actionPerformed(ActionEvent e)
            {
                TableCellListener tcl = (TableCellListener)e.getSource();
                if(tcl.getColumn()==0 ){
                    agregarDetalle(tcl.getNewValue(),tcl.getRow());
                    if(tcl.getOldValue()==null){
                        agregarFila();
                    }
                }else{
                    actualizarDetalle(tcl.getRow());
                }
                
                actualizarSumas();
                
            }
        };
        
        listenerTabla = new TableCellListener(this.tablaDetalles,action);
    }
    
    void agregarFila(){
        DefaultTableModel  modelo = (DefaultTableModel) this.tablaDetalles.getModel();
        filas++;
        modelo.setRowCount(filas);
    }
    
    void agregarDetalle(Object pk,Integer fila){
        int llave = a_entero(pk.toString());
        Inventarios prod = (Inventarios)Operaciones.getInstancia().buscar(llave, new Inventarios());
        if(prod!=null){
            this.tablaDetalles.getModel().setValueAt(prod.getDescripcion(), fila, 1);
            this.tablaDetalles.getModel().setValueAt(prod.getPrecioCompra(), fila, 2);
            this.tablaDetalles.getModel().setValueAt(1, fila, 3);
            this.tablaDetalles.getModel().setValueAt(prod.getExistencias(), fila, 4);
            this.tablaDetalles.getModel().setValueAt(0, fila, 5);
            this.tablaDetalles.getModel().setValueAt(0, fila, 6);
            this.tablaDetalles.getModel().setValueAt(prod.getPrecioCompra(), fila, 7);
        }else{
            JOptionPane.showMessageDialog(null, "No se encontro el producto ");
            this.tablaDetalles.getModel().setValueAt("", fila, 0);
            this.tablaDetalles.getModel().setValueAt("", fila, 1);
            this.tablaDetalles.getModel().setValueAt("", fila, 2);
            this.tablaDetalles.getModel().setValueAt("", fila, 3);
            this.tablaDetalles.getModel().setValueAt("", fila, 4);
            this.tablaDetalles.getModel().setValueAt("", fila, 5);
            this.tablaDetalles.getModel().setValueAt("", fila, 6);
            this.tablaDetalles.getModel().setValueAt("", fila, 7);
        }
        actualizarSumas();
    }
            
   
    
    
    void actualizarDetalle(Integer fila){
       int cantidad = a_entero(this.tablaDetalles.getModel().getValueAt(fila, 3).toString());
        Double existencias = Double.parseDouble(this.tablaDetalles.getModel().getValueAt(fila, 4).toString());
        Double Precio = Double.parseDouble(this.tablaDetalles.getModel().getValueAt(fila, 2).toString());
        if(cantidad>0){
            Double total = cantidad*Precio;
            this.tablaDetalles.getModel().setValueAt(total, fila, 7);
        }else{
            JOptionPane.showMessageDialog(null, "Dato no valido");
            this.tablaDetalles.getModel().setValueAt(1, fila, 3);
            this.tablaDetalles.getModel().setValueAt(Precio, fila, 7);
        }

    } 
    
    
    void actualizarSumas(){
        Double c_ex=0.0;
        Double c_no=0.0;
        Double c_af=0.0;
        
        for(int i=0;i<tablaDetalles.getRowCount()-1;i++){
            int id = a_entero(this.tablaDetalles.getModel().getValueAt(i, 0).toString());
            if(id>0){
                c_ex += Double.parseDouble(this.tablaDetalles.getModel().getValueAt(i, 5).toString());
                c_no += Double.parseDouble(this.tablaDetalles.getModel().getValueAt(i, 6).toString());
                c_af += Double.parseDouble(this.tablaDetalles.getModel().getValueAt(i, 7).toString());
            }
        }
        txtSubtotal.setText(c_af.toString());
        txtTotalPagar.setText(c_af.toString());
        txtIVA.setText("0.0");//dudas aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
        txtIVAPercibido.setText("0.0");
        txtIVARetenido.setText("0.0");
    }

    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txtFecha = new javax.swing.JSpinner();
        jLabel6 = new javax.swing.JLabel();
        txtProveedor = new javax.swing.JTextField();
        btnBuscarProveedor = new javax.swing.JButton();
        btnSeleccionarProveedor = new javax.swing.JButton();
        txtNombresProveedor = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtApellidosProveedor = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtEmailProveedor = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtCorrelativo = new javax.swing.JTextField();
        txtTelefonoProveedor = new javax.swing.JFormattedTextField();
        jLabel2 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaDetalles = new javax.swing.JTable();
        btnEliminarDetalle = new javax.swing.JButton();
        btnBuscarProd = new javax.swing.JButton();
        btnNuevoProducto = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        txtIVA = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtSubtotal = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txtIVARetenido = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txtIVAPercibido = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txtTotalPagar = new javax.swing.JTextField();
        btnProcesar = new javax.swing.JButton();

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos Factura"));

        jLabel5.setText("Fecha : ");

        txtFecha.setModel(new javax.swing.SpinnerDateModel());

        jLabel6.setText("Proveedor:");

        txtProveedor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtProveedorKeyTyped(evt);
            }
        });

        btnBuscarProveedor.setText("Buscar");
        btnBuscarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarProveedorActionPerformed(evt);
            }
        });

        btnSeleccionarProveedor.setText("Seleccionar");
        btnSeleccionarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeleccionarProveedorActionPerformed(evt);
            }
        });

        txtNombresProveedor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombresProveedorKeyTyped(evt);
            }
        });

        jLabel8.setText("Teléfono:");

        jLabel9.setText("Apellidos:");

        txtApellidosProveedor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtApellidosProveedorKeyTyped(evt);
            }
        });

        jLabel7.setText("Nombres: ");

        txtEmailProveedor.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtEmailProveedorFocusLost(evt);
            }
        });
        txtEmailProveedor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtEmailProveedorKeyTyped(evt);
            }
        });

        jLabel10.setText("Email:");

        jLabel11.setText("Correlativo:");

        txtCorrelativo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCorrelativoKeyTyped(evt);
            }
        });

        try {
            txtTelefonoProveedor.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtTelefonoProveedor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTelefonoProveedorKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBuscarProveedor)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSeleccionarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtNombresProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtApellidosProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTelefonoProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtEmailProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCorrelativo, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel11)
                        .addComponent(txtCorrelativo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscarProveedor)
                    .addComponent(btnSeleccionarProveedor))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel10)
                        .addComponent(txtEmailProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtNombresProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9)
                        .addComponent(txtApellidosProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel8)
                        .addComponent(jLabel7)
                        .addComponent(txtTelefonoProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(98, 98, 98))
        );

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Nueva Compra");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Detalles"));

        tablaDetalles.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Código", "Descripción", "P. Unitario", "Cantidad", "Existencias", "V. Exenta", "V. No sujetas", "Ventas Afectas"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, false, true, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaDetalles.setRowMargin(2);
        jScrollPane1.setViewportView(tablaDetalles);
        if (tablaDetalles.getColumnModel().getColumnCount() > 0) {
            tablaDetalles.getColumnModel().getColumn(0).setPreferredWidth(6);
            tablaDetalles.getColumnModel().getColumn(1).setPreferredWidth(20);
            tablaDetalles.getColumnModel().getColumn(2).setPreferredWidth(7);
            tablaDetalles.getColumnModel().getColumn(5).setPreferredWidth(7);
            tablaDetalles.getColumnModel().getColumn(6).setPreferredWidth(7);
            tablaDetalles.getColumnModel().getColumn(7).setPreferredWidth(10);
        }

        btnEliminarDetalle.setText("Eliminar");
        btnEliminarDetalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarDetalleActionPerformed(evt);
            }
        });

        btnBuscarProd.setText("Buscar Producto");
        btnBuscarProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarProdActionPerformed(evt);
            }
        });

        btnNuevoProducto.setText("Nuevo Producto");
        btnNuevoProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoProductoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(btnBuscarProd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnNuevoProducto)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1220, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(btnEliminarDetalle)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBuscarProd)
                    .addComponent(btnNuevoProducto))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnEliminarDetalle))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos Contables"));

        jLabel17.setText("IVA : ");

        txtIVA.setEditable(false);
        txtIVA.setText("0.13");

        jLabel18.setText("Subtotal : ");

        txtSubtotal.setEnabled(false);

        jLabel19.setText("IVA Retenido : ");

        txtIVARetenido.setEditable(false);

        jLabel20.setText("IVA Percibido : ");

        txtIVAPercibido.setEditable(false);

        jLabel21.setText("Total a pagar : ");

        txtTotalPagar.setEnabled(false);

        btnProcesar.setText("Procesar Compra");
        btnProcesar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcesarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel21)
                    .addComponent(jLabel18))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtSubtotal)
                    .addComponent(txtTotalPagar, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btnProcesar, javax.swing.GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE)
                        .addGap(787, 787, 787))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtIVA, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel19)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtIVARetenido, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtIVAPercibido, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(txtSubtotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17)
                    .addComponent(txtIVA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19)
                    .addComponent(txtIVARetenido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20)
                    .addComponent(txtIVAPercibido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(txtTotalPagar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnProcesar))
                .addGap(0, 13, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarProveedorActionPerformed
        // TODO add your handling code here:

        if(this.txtProveedor.getText().length()>0 && a_entero_valido(this.txtProveedor.getText())){
            proveedor = (Proveedores)Operaciones.getInstancia().buscar(Integer.parseInt(this.txtProveedor.getText()), new Proveedores());
            if(proveedor!=null){
                this.txtNombresProveedor.setText(proveedor.getNombres());
                this.txtApellidosProveedor.setText(proveedor.getApellidos());
                this.txtTelefonoProveedor.setText(proveedor.getTelefono());
                this.txtEmailProveedor.setText(proveedor.getEmail());
            }else{
                JOptionPane.showMessageDialog(null, "No se encontro el proveedor ");
            }
        }else{
            this.limpiarProveedor();
        }

    }//GEN-LAST:event_btnBuscarProveedorActionPerformed

    private void btnProcesarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcesarActionPerformed
        // TODO add your handling code here:
        if (txtCorrelativo.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE); 
        }else if (txtProveedor.getText().isEmpty()){
             JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else if (txtNombresProveedor.getText().isEmpty()){
             JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else if (txtApellidosProveedor.getText().isEmpty()){
             JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else if (txtTelefonoProveedor.getText().isEmpty()){
             JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else if (txtEmailProveedor.getText().isEmpty()){
             JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else{
        
        //****************Agregando al log******************************
                    Log l = new Log();
                    l.setDescripcion("Insertando registros tabla compra");
                    l.setOperacion("Inserción");
                    l.setFecha(new Date());
                    l.setUsuarios((Usuarios)Operaciones.getInstancia().buscar(SingleUser.getInstancia().getIdUsuario(), new Usuarios()));
                    Operaciones.getInstancia().guardar(l,false);
        //****************Agregando al log******************************
        
        guardarFactura();
        
        
        }
        
    }//GEN-LAST:event_btnProcesarActionPerformed

    
    
   
    private void limpiarDatos(){
        Double c_af=0.0;
        
        this.txtCorrelativo.setText("");
        this.txtProveedor.setText("");
        this.txtNombresProveedor.setText("");
        this.txtApellidosProveedor.setText("");
        this.txtTelefonoProveedor.setText("");
        this.txtEmailProveedor.setText("");
        this.txtSubtotal.setText(c_af.toString());
        this.txtTotalPagar.setText(c_af.toString());
        //this.tablaDetalles.setModel();
    }
    
    
            
    private void btnBuscarProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarProdActionPerformed
        // TODO add your handling code here:
        dialogoBuscarInventario dialogo = new dialogoBuscarInventario(new Frame(), true);
        dialogo.setVisible(true);
        Inventarios c = dialogo.c;
        if(c!=null){
            agregarFila();
            tablaDetalles.getModel().setValueAt(c.getIdInventarios(),filas-2, 0);
            agregarDetalle(c.getIdInventarios(),filas-2);
        }
    }//GEN-LAST:event_btnBuscarProdActionPerformed

    private void btnEliminarDetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarDetalleActionPerformed
        // TODO add your handling code here:
        
        //****************Agregando al log******************************
                    Log l = new Log();
                    l.setDescripcion("Eliminando registros tabla compra");
                    l.setOperacion("Eliminación");
                    l.setFecha(new Date());
                    l.setUsuarios((Usuarios)Operaciones.getInstancia().buscar(SingleUser.getInstancia().getIdUsuario(), new Usuarios()));
                    Operaciones.getInstancia().guardar(l,false);
        //****************Agregando al log******************************
        
        int selectedRow = tablaDetalles.getSelectedRow();
        
        DefaultTableModel model = (DefaultTableModel) this.tablaDetalles.getModel();
        if(model.getRowCount() >  1 && selectedRow!=-1) {
            if(selectedRow!=model.getRowCount()-1){
                model.removeRow(selectedRow);
                filas--;
            }
        }
        actualizarSumas();

    }//GEN-LAST:event_btnEliminarDetalleActionPerformed

    private void btnSeleccionarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeleccionarProveedorActionPerformed
        // TODO add your handling code here:
        dialogoBuscarProveedores dialogo = new dialogoBuscarProveedores(new Frame(), true);
        dialogo.setVisible(true);
        Proveedores p = dialogo.p;
        if(p!=null){
            txtProveedor.setText(""+p.getIdProveedores());
            txtNombresProveedor.setText(p.getNombres());
            txtApellidosProveedor.setText(p.getApellidos());
            txtTelefonoProveedor.setText(p.getTelefono());
            txtEmailProveedor.setText(p.getEmail());
        }

    }//GEN-LAST:event_btnSeleccionarProveedorActionPerformed

    private void btnNuevoProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoProductoActionPerformed
        // TODO add your handling code here:
        
        //****************Agregando al log******************************
                    Log l = new Log();
                    l.setDescripcion("Insertando nuevo registros tabla compra");
                    l.setOperacion("Inserción");
                    l.setFecha(new Date());
                    l.setUsuarios((Usuarios)Operaciones.getInstancia().buscar(SingleUser.getInstancia().getIdUsuario(), new Usuarios()));
                    Operaciones.getInstancia().guardar(l,false);
        //****************Agregando al log******************************
        
        dialogoNuevoProducto dialogo = new dialogoNuevoProducto(new Frame(),true);
        dialogo.setVisible(true);
        Inventarios c = dialogo.i;
        if(c!=null){
            agregarFila();
            tablaDetalles.getModel().setValueAt(c.getIdInventarios(),filas-2, 0);
            agregarDetalle(c.getIdInventarios(),filas-2);
        }
    }//GEN-LAST:event_btnNuevoProductoActionPerformed

    
    
    
    
    private void txtCorrelativoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCorrelativoKeyTyped
        // TODO add your handling code here:
         int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada    
         if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtCorrelativo.transferFocus();
        }
    }//GEN-LAST:event_txtCorrelativoKeyTyped

    private void txtProveedorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtProveedorKeyTyped
        // TODO add your handling code here:
         int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada    
         if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtProveedor.transferFocus();
        }
    }//GEN-LAST:event_txtProveedorKeyTyped

    private void txtNombresProveedorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombresProveedorKeyTyped
        // TODO add your handling code here:
        int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada   
        
         if (k > 47 && k < 58) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar numeros!!!", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtNombresProveedor.transferFocus();
        }
    }//GEN-LAST:event_txtNombresProveedorKeyTyped

    private void txtApellidosProveedorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidosProveedorKeyTyped
        // TODO add your handling code here:
        int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada   
        
         if (k > 47 && k < 58) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar numeros!!!", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtApellidosProveedor.transferFocus();
        }
    }//GEN-LAST:event_txtApellidosProveedorKeyTyped

    
    
     //Metodo para validar email
    public boolean isEmail(String correo){
        Pattern pat = null;
        Matcher mat = null;
        pat = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        mat = pat.matcher(correo);
        if (mat.find()){
            return true;
        }else{
            return false;
        }
    }
    
    private void txtEmailProveedorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEmailProveedorKeyTyped
        // TODO add your handling code here:
       
    }//GEN-LAST:event_txtEmailProveedorKeyTyped

    private void txtTelefonoProveedorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefonoProveedorKeyTyped
        // TODO add your handling code here:
         int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada    
         if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtTelefonoProveedor.transferFocus();
        }
    }//GEN-LAST:event_txtTelefonoProveedorKeyTyped

    private void txtEmailProveedorFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtEmailProveedorFocusLost
        // TODO add your handling code here:
        if (isEmail(txtEmailProveedor.getText())){
            
        }else{
            JOptionPane.showMessageDialog(null, "Email incorrecto", "Validar email", JOptionPane.INFORMATION_MESSAGE);
            txtEmailProveedor.requestFocus();
        }
    }//GEN-LAST:event_txtEmailProveedorFocusLost

    
    
    
    
    
    
    private void guardarFactura(){
        if(valido()){
            
            
            Compras factura = new Compras();
            factura.setProveedores((Proveedores) Operaciones.getInstancia().buscar( a_entero(txtProveedor.getText()) , new Proveedores()) );
            factura.setFecha((Date)txtFecha.getValue() );
            factura.setCorrelativo(txtCorrelativo.getText());
            factura.setUsuarios((Usuarios)Operaciones.getInstancia().buscar(SingleUser.getInstancia().getIdUsuario(), new Usuarios()));
            factura.setTotal(Double.parseDouble(txtTotalPagar.getText()));
            factura.setCondicionPago("contado");
            factura.setEstatus("pagado");
            factura.setSubTotal(Double.parseDouble(txtSubtotal.getText()));
            factura.setIva(Double.parseDouble(txtIVA.getText()));
            factura.setIvaPercibido(Double.parseDouble(txtIVAPercibido.getText()));
            factura.setIvaRetenido(Double.parseDouble(txtIVARetenido.getText()));
            if(Operaciones.getInstancia().guardar(factura)){
                for(int i=0;i<tablaDetalles.getRowCount()-1;i++){
                    int id = a_entero(this.tablaDetalles.getModel().getValueAt(i, 0).toString());
                    if(id>0){
                        DetallesCompras detalle = new DetallesCompras();
                        detalle.setInventarios((Inventarios)Operaciones.getInstancia().buscar(id, new Inventarios()));
                        detalle.setCompras(factura);
                        detalle.setCantidad(Double.parseDouble(this.tablaDetalles.getModel().getValueAt(i, 3).toString()));
                        detalle.setPrecioUnitario(Double.parseDouble(this.tablaDetalles.getModel().getValueAt(i, 2).toString()));
                        detalle.setDescripcion(this.tablaDetalles.getModel().getValueAt(i, 1).toString());
                        detalle.setVentasExentas(Double.parseDouble(this.tablaDetalles.getModel().getValueAt(i, 5).toString()));
                        detalle.setVentasNoSujetas(Double.parseDouble(this.tablaDetalles.getModel().getValueAt(i, 6).toString()));
                        detalle.setVentasGravadas(Double.parseDouble(this.tablaDetalles.getModel().getValueAt(i, 7).toString()));
                        Operaciones.getInstancia().guardar(detalle,false);
                    }
                }
                //mensaje de mostrar si imprimir
                int reply = JOptionPane.showConfirmDialog(null, "¿Desea imprimir la factura?", "Informacion", JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION) {
                    rptFacturaCompra rpt = new rptFacturaCompra();
                    rpt.id = factura.getIdCompras();
                    rpt.reporteMostrar();
                }
                JTabbedPane padre = ((JTabbedPane)SwingUtilities.getAncestorOfClass(JTabbedPane.class, panelCompra.this));
                int selected = padre.getSelectedIndex();
                padre.remove(selected);
                
            }else{
            }
        }else{
            JOptionPane.showMessageDialog(null, "Datos incompletos");
        }
    }
    
    
    private boolean valido(){
        if(txtProveedor.getText().equals("") || txtTotalPagar.getText().equals("") || txtTotalPagar.getText().equals("0.0")){
            return false;
        }
        return true;
    }

    
    
    private void limpiarProveedor(){
        this.txtProveedor.setText("");
        this.txtNombresProveedor.setText("");
        this.txtApellidosProveedor.setText("");
        this.txtTelefonoProveedor.setText("");
        this.txtEmailProveedor.setText("");
        
    }    

    
    boolean a_entero_valido(String dato){
        try{
            if(Integer.parseInt(dato)>0){
                return true;
            }else{
                JOptionPane.showMessageDialog(new JFrame(),"Digite números validos","Datos invalidos",JOptionPane.WARNING_MESSAGE);
                return false;
            }
        }catch(Exception ex){
            JOptionPane.showMessageDialog(new JFrame(),"Digite unicamente números","Datos invalidos",JOptionPane.WARNING_MESSAGE);
            return false;
        }
    }
    
    int a_entero(String dato){
        try{
            return Integer.parseInt(dato);
        }catch(Exception Ex){
            return -1;
        }
    }

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarProd;
    private javax.swing.JButton btnBuscarProveedor;
    private javax.swing.JButton btnEliminarDetalle;
    private javax.swing.JButton btnNuevoProducto;
    private javax.swing.JButton btnProcesar;
    private javax.swing.JButton btnSeleccionarProveedor;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaDetalles;
    private javax.swing.JTextField txtApellidosProveedor;
    private javax.swing.JTextField txtCorrelativo;
    private javax.swing.JTextField txtEmailProveedor;
    private javax.swing.JSpinner txtFecha;
    private javax.swing.JTextField txtIVA;
    private javax.swing.JTextField txtIVAPercibido;
    private javax.swing.JTextField txtIVARetenido;
    private javax.swing.JTextField txtNombresProveedor;
    private javax.swing.JTextField txtProveedor;
    private javax.swing.JTextField txtSubtotal;
    private javax.swing.JFormattedTextField txtTelefonoProveedor;
    private javax.swing.JTextField txtTotalPagar;
    // End of variables declaration//GEN-END:variables
}
