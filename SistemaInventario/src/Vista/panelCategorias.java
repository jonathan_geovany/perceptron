/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.Operaciones;
import Controlador.SingleUser;
import Modelo.Categorias;
import Modelo.Log;
import Modelo.Subcategorias;
import Modelo.Usuarios;
import java.awt.Component;
import java.awt.PopupMenu;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jonathan
 */
public class panelCategorias extends javax.swing.JPanel {

    /**
     * Creates new form panelSubcategorias
     */
    DefaultTableModel tablaModelo;
    boolean esModificar;
    boolean esModificar2; 
    DefaultTableModel tablaModeloSub;
    public panelCategorias() {
        
        initComponents();
        Operaciones.getInstancia().setHabilitado(panelDatos,false);
        tablaModelo = (DefaultTableModel)tablaCategorias.getModel();
        tablaModeloSub = (DefaultTableModel)tablaSubcategorias.getModel();
        actualizarTabla();
        tablaCategorias.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (tablaCategorias.getSelectedRow() > -1) {
                    //actualizar textboxes
                    txtId.setText(tablaCategorias.getValueAt(tablaCategorias.getSelectedRow(), 0).toString());
                    txtNombre.setText(tablaCategorias.getValueAt(tablaCategorias.getSelectedRow(), 1).toString());
                    actualizarTabla2(Integer.parseInt(txtId.getText()));
                }
            }
        });
        Operaciones.getInstancia().setHabilitado(panelDatosSub,false);
        
        
        tablaSubcategorias.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (tablaSubcategorias.getSelectedRow() > -1) {
                    //actualizar textboxes
                    txtIdSub.setText(tablaSubcategorias.getValueAt(tablaSubcategorias.getSelectedRow(), 0).toString());
                    txtNombreSub.setText(tablaSubcategorias.getValueAt(tablaSubcategorias.getSelectedRow(), 1).toString());
                }
            }
    });
    }
    
    private void limpiar(){
        txtId.setText("");
        txtNombre.setText("");
    }
    
    private void actualizarTabla(){
        List<Object[]> lista;
        lista = Operaciones.getInstancia().consulta("select * from categorias");
        tablaModelo.setRowCount(0);
        lista.stream().forEach((i)->{tablaModelo.addRow(i);});
        tablaCategorias.setModel(tablaModelo);
    }
    
    private void insertar(){
        Categorias c= new Categorias();
        
        if (txtNombre.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else{
            c.setNombreCategoria(txtNombre.getText());
            c.setTotalProducto(0.0);
            Operaciones.getInstancia().guardar(c);
            Operaciones.getInstancia().setHabilitado(panelDatos,false);
            limpiar();
        }
            actualizarTabla();
    }
    
    
    private void modificar(){
        Categorias c= new Categorias();
        c.setIdCategorias( Integer.parseInt(txtId.getText()));
        c.setNombreCategoria(txtNombre.getText());
        c.setTotalProducto(Double.parseDouble(tablaCategorias.getValueAt(tablaCategorias.getSelectedRow(), 2).toString()));
        Operaciones.getInstancia().guardar(c);
        Operaciones.getInstancia().setHabilitado(panelDatos,false);
        limpiar();
        actualizarTabla();
    }
    
    
    
    private void limpiar2(){
        txtIdSub.setText("");
        txtNombreSub.setText("");
    }
    
    private void actualizarTabla2(int pos){
        List<Object[]> lista;
        lista = Operaciones.getInstancia().consulta("select * from subcategorias s where s.idCategoria= "+pos);
        tablaModeloSub.setRowCount(0);
        lista.stream().forEach((i)->{tablaModeloSub.addRow(i);});
        tablaSubcategorias.setModel(tablaModeloSub);
    }
    
    private void insertar2(){
        Subcategorias c= new Subcategorias();
        
        if (txtNombreSub.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else{
            c.setNombreSubCategoria(txtNombreSub.getText());
            c.setCategorias( (Categorias)Operaciones.getInstancia().buscar(Integer.parseInt(txtId.getText()) , new Categorias()) );
            c.setTotalProducto(0.0);
            Operaciones.getInstancia().guardar(c);
            Operaciones.getInstancia().setHabilitado(panelDatosSub,false);
            limpiar2();
        }
            actualizarTabla2(Integer.parseInt(txtId.getText()));
    }
    
    
    private void modificar2(){
        Subcategorias c= new Subcategorias();
        c.setIdSubcategorias(Integer.parseInt(txtIdSub.getText()));
        c.setNombreSubCategoria(txtNombreSub.getText());
        c.setCategorias( (Categorias)Operaciones.getInstancia().buscar(Integer.parseInt(txtId.getText()) , new Categorias()) );
        c.setTotalProducto(Double.parseDouble(tablaSubcategorias.getValueAt(tablaSubcategorias.getSelectedRow(), 2).toString() ));
        Operaciones.getInstancia().guardar(c);
        Operaciones.getInstancia().setHabilitado(panelDatosSub,false);
        limpiar2();
        actualizarTabla2(Integer.parseInt(txtId.getText()));
        
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        panelDatos = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaCategorias = new javax.swing.JTable();
        btnModificar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaSubcategorias = new javax.swing.JTable();
        btnModificarSub = new javax.swing.JButton();
        btnEliminarSub = new javax.swing.JButton();
        btnNuevoSub = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        panelDatosSub = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        txtIdSub = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtNombreSub = new javax.swing.JTextField();
        btnGuardarSub = new javax.swing.JButton();
        btnCancelarSub = new javax.swing.JButton();

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Categorias");

        panelDatos.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos categoria"));

        jLabel3.setText("id : ");

        txtId.setEditable(false);
        txtId.setEnabled(false);
        txtId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdActionPerformed(evt);
            }
        });

        jLabel6.setText("Nombre : ");

        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreKeyTyped(evt);
            }
        });

        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelDatosLayout = new javax.swing.GroupLayout(panelDatos);
        panelDatos.setLayout(panelDatosLayout);
        panelDatosLayout.setHorizontalGroup(
            panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDatosLayout.createSequentialGroup()
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDatosLayout.createSequentialGroup()
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtNombre, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                            .addComponent(txtId)))
                    .addGroup(panelDatosLayout.createSequentialGroup()
                        .addComponent(btnGuardar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelDatosLayout.setVerticalGroup(
            panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDatosLayout.createSequentialGroup()
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardar)
                    .addComponent(btnCancelar))
                .addGap(95, 95, 95))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Listado categorias"));

        tablaCategorias.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Nombre", "Productos"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaCategorias.setRowMargin(2);
        jScrollPane1.setViewportView(tablaCategorias);

        btnModificar.setText("Modificar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnNuevo.setText("Nueva");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(btnNuevo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModificar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEliminar)
                .addContainerGap(124, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnModificar)
                    .addComponent(btnEliminar)
                    .addComponent(btnNuevo)))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Listado subcategorias"));

        tablaSubcategorias.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Nombre", "Productos"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaSubcategorias.setRowMargin(2);
        jScrollPane3.setViewportView(tablaSubcategorias);

        btnModificarSub.setText("Modificar");
        btnModificarSub.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarSubActionPerformed(evt);
            }
        });

        btnEliminarSub.setText("Eliminar");
        btnEliminarSub.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarSubActionPerformed(evt);
            }
        });

        btnNuevoSub.setText("Nueva");
        btnNuevoSub.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoSubActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(btnNuevoSub)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModificarSub)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEliminarSub)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 484, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnModificarSub)
                    .addComponent(btnEliminarSub)
                    .addComponent(btnNuevoSub)))
        );

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setText("Subcategorias");

        panelDatosSub.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos subcategoria"));

        jLabel9.setText("id : ");

        txtIdSub.setEditable(false);
        txtIdSub.setEnabled(false);
        txtIdSub.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdSubActionPerformed(evt);
            }
        });

        jLabel10.setText("Nombre : ");

        txtNombreSub.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreSubKeyTyped(evt);
            }
        });

        btnGuardarSub.setText("Guardar");
        btnGuardarSub.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarSubActionPerformed(evt);
            }
        });

        btnCancelarSub.setText("Cancelar");
        btnCancelarSub.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarSubActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelDatosSubLayout = new javax.swing.GroupLayout(panelDatosSub);
        panelDatosSub.setLayout(panelDatosSubLayout);
        panelDatosSubLayout.setHorizontalGroup(
            panelDatosSubLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDatosSubLayout.createSequentialGroup()
                .addGroup(panelDatosSubLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDatosSubLayout.createSequentialGroup()
                        .addGroup(panelDatosSubLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelDatosSubLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtNombreSub, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                            .addComponent(txtIdSub)))
                    .addGroup(panelDatosSubLayout.createSequentialGroup()
                        .addComponent(btnGuardarSub)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCancelarSub, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelDatosSubLayout.setVerticalGroup(
            panelDatosSubLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDatosSubLayout.createSequentialGroup()
                .addGroup(panelDatosSubLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtIdSub, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelDatosSubLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtNombreSub, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDatosSubLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardarSub)
                    .addComponent(btnCancelarSub))
                .addGap(95, 95, 95))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2)
                    .addComponent(panelDatos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(523, 523, 523))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(panelDatosSub, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(115, 115, 115))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(panelDatos, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(panelDatosSub, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(62, Short.MAX_VALUE))
        );

        panelDatos.getAccessibleContext().setAccessibleName("Datos subcategoria");
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        if(esModificar){
            
            //****************Agregando al log******************************
                    Log l = new Log();
                    l.setDescripcion("Modificando registros tabla categorias");
                    l.setOperacion("Modificación");
                    l.setFecha(new Date());
                    l.setUsuarios((Usuarios)Operaciones.getInstancia().buscar(SingleUser.getInstancia().getIdUsuario(), new Usuarios()));
                    Operaciones.getInstancia().guardar(l,false);
            //****************Agregando al log******************************
            
            modificar();
        }else{
            
            //****************Agregando al log******************************
                    Log l = new Log();
                    l.setDescripcion("Insertando registros tabla categorias");
                    l.setOperacion("Inserción");
                    l.setFecha(new Date());
                    l.setUsuarios((Usuarios)Operaciones.getInstancia().buscar(SingleUser.getInstancia().getIdUsuario(), new Usuarios()));
                    Operaciones.getInstancia().guardar(l,false);
            //****************Agregando al log******************************
            
            insertar();
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        Operaciones.getInstancia().setHabilitado(panelDatos,false);
        limpiar();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        Operaciones.getInstancia().setHabilitado(panelDatos,true);
        esModificar=false;
        limpiar();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void txtIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIdActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        // TODO add your handling code here:
        esModificar=true;
        Operaciones.getInstancia().setHabilitado(panelDatos,true);
        
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        // TODO add your handling code here:
        if(tablaCategorias.getRowCount()>0){
            
            //****************Agregando al log******************************
                    Log l = new Log();
                    l.setDescripcion("Eliminando registros tabla categorias");
                    l.setOperacion("Eliminación");
                    l.setFecha(new Date());
                    l.setUsuarios((Usuarios)Operaciones.getInstancia().buscar(SingleUser.getInstancia().getIdUsuario(), new Usuarios()));
                    Operaciones.getInstancia().guardar(l,false);
            //****************Agregando al log******************************
            
            Operaciones.getInstancia().eliminar(Integer.parseInt(txtId.getText()),new Categorias());
            actualizarTabla();
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnModificarSubActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarSubActionPerformed
        // TODO add your handling code here:
        esModificar2=true;
        Operaciones.getInstancia().setHabilitado(panelDatosSub,true);

    }//GEN-LAST:event_btnModificarSubActionPerformed

    private void btnEliminarSubActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarSubActionPerformed
        // TODO add your handling code here:
        if(tablaSubcategorias.getRowCount()>0){
            
            //****************Agregando al log******************************
                    Log l = new Log();
                    l.setDescripcion("Modificando registros tabla subcategorias");
                    l.setOperacion("Modificación");
                    l.setFecha(new Date());
                    l.setUsuarios((Usuarios)Operaciones.getInstancia().buscar(SingleUser.getInstancia().getIdUsuario(), new Usuarios()));
                    Operaciones.getInstancia().guardar(l,false);
            //****************Agregando al log******************************
            
            Operaciones.getInstancia().eliminar(Integer.parseInt(txtIdSub.getText()),new Subcategorias());
            actualizarTabla2(Integer.parseInt(txtId.getText()));
        }
    }//GEN-LAST:event_btnEliminarSubActionPerformed

    private void btnNuevoSubActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoSubActionPerformed
        // TODO add your handling code here:
        Operaciones.getInstancia().setHabilitado(panelDatosSub,true);
        esModificar2=false;
        limpiar2();
    }//GEN-LAST:event_btnNuevoSubActionPerformed

    private void txtIdSubActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdSubActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIdSubActionPerformed

    private void btnGuardarSubActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarSubActionPerformed
        // TODO add your handling code here:
        if(esModificar2){
            
            //****************Agregando al log******************************
                    Log l = new Log();
                    l.setDescripcion("Modificando registros tabla subcategorias");
                    l.setOperacion("Modificación");
                    l.setFecha(new Date());
                    l.setUsuarios((Usuarios)Operaciones.getInstancia().buscar(SingleUser.getInstancia().getIdUsuario(), new Usuarios()));
                    Operaciones.getInstancia().guardar(l,false);
            //****************Agregando al log******************************
            
            modificar2();
        }else{
            
            //****************Agregando al log******************************
                    Log l = new Log();
                    l.setDescripcion("Inserción registros tabla subcategorias");
                    l.setOperacion("Inserción");
                    l.setFecha(new Date());
                    l.setUsuarios((Usuarios)Operaciones.getInstancia().buscar(SingleUser.getInstancia().getIdUsuario(), new Usuarios()));
                    Operaciones.getInstancia().guardar(l,false);
            //****************Agregando al log******************************
            
            insertar2();
        }
    }//GEN-LAST:event_btnGuardarSubActionPerformed

    private void btnCancelarSubActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarSubActionPerformed
        // TODO add your handling code here:
        Operaciones.getInstancia().setHabilitado(panelDatosSub,false);
        limpiar2();
    }//GEN-LAST:event_btnCancelarSubActionPerformed

    
    
    /************************VALIDAR QUE CAMPO SEA SOLO LETRA O SOLO NUMEROS*****************************/
    private void txtNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyTyped
        // TODO add your handling code here:
        int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada   
        
         if (k > 47 && k < 58) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar numeros!!!", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtNombre.transferFocus();
        }
    }//GEN-LAST:event_txtNombreKeyTyped

    private void txtNombreSubKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreSubKeyTyped
        // TODO add your handling code here:
        int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada   
        
         if (k > 47 && k < 58) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar numeros!!!", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtNombreSub.transferFocus();
        }
    }//GEN-LAST:event_txtNombreSubKeyTyped
    /************************VALIDAR QUE CAMPO SEA SOLO LETRA O SOLO NUMEROS*****************************/
    
    
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCancelarSub;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnEliminarSub;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnGuardarSub;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnModificarSub;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnNuevoSub;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JPanel panelDatos;
    private javax.swing.JPanel panelDatosSub;
    private javax.swing.JTable tablaCategorias;
    private javax.swing.JTable tablaSubcategorias;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtIdSub;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNombreSub;
    // End of variables declaration//GEN-END:variables
}
