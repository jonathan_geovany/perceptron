/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.Operaciones;
import Controlador.Seguridad;
import Controlador.SingleUser;
import Modelo.Log;
import Modelo.Usuarios;
import Modelo.Roles;
import java.awt.Color;
import java.awt.Component;
import java.awt.PopupMenu;
import java.awt.event.KeyEvent;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.DefaultCellEditor;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author Jonathan
 */
class PasswordCellRenderer extends DefaultTableCellRenderer {

    private static final String ASTERISKS = "************************";

    @Override
    public Component getTableCellRendererComponent(JTable arg0, Object arg1, boolean arg2, boolean arg3, int arg4, int arg5) {
        super.getTableCellRendererComponent(arg0,arg1,arg2,arg3,arg4,arg5);
        int length =0;
        if (arg1 instanceof String) {
            length =  ((String) arg1).length();
        } else if (arg1 instanceof char[]) {
            length = ((char[])arg1).length;
        }
        setText(asterisks(length));
        return this;
    }

    private String asterisks(int length) {
        if (length > ASTERISKS.length()) {
            StringBuilder sb = new StringBuilder(length);
            for (int i = 0; i < length; i++) {
                sb.append('*');
            }
            return sb.toString();
        } else {
            return ASTERISKS.substring(0, length);
        }
    }
}


public class panelUsuarios extends javax.swing.JPanel {

    /**
     * Creates new form panelSubcategorias
     */
    //crear lista para controla el rol
    List<Roles> lstRol;
    
    
    DefaultTableModel tablaModelo;
    boolean esModificar;
    Seguridad seguridad;
    public panelUsuarios() {
        initComponents();
        seguridad = new Seguridad();
        //inicializar lista
        lstRol = (List<Roles>)(List<?>)Operaciones.getInstancia().lista(new Roles());
        for(int i=0;i< lstRol.size();i++){
            cmbRol.addItem(lstRol.get(i).getRol());
        }
        
        Operaciones.getInstancia().setHabilitado(panelDatos,false);
        tablaModelo = (DefaultTableModel)tablaUsuarios.getModel();
        actualizarTabla();
        tablaUsuarios.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (tablaUsuarios.getSelectedRow() > -1) {
                    //actualizar textboxes
                    txtUsuario.setText(tablaUsuarios.getValueAt(tablaUsuarios.getSelectedRow(), 0).toString());
                    txtNombre.setText(tablaUsuarios.getValueAt(tablaUsuarios.getSelectedRow(), 1).toString());
                    txtApellido.setText(tablaUsuarios.getValueAt(tablaUsuarios.getSelectedRow(), 2).toString());   
                    txtClave.setText(tablaUsuarios.getValueAt(tablaUsuarios.getSelectedRow(),4).toString());   
                    
                    //txtcmbRol. (tablaUsuarios.getValueAt(tablaUsuarios.getSelectedRow(), 4).toString());   
                    
                    txtDireccion.setText(tablaUsuarios.getValueAt(tablaUsuarios.getSelectedRow(), 5).toString());   
                    txtTelefono.setText(tablaUsuarios.getValueAt(tablaUsuarios.getSelectedRow(), 6).toString()); 
                    txtDUI.setText(tablaUsuarios.getValueAt(tablaUsuarios.getSelectedRow(), 8).toString());
                    txtNIT.setText(tablaUsuarios.getValueAt(tablaUsuarios.getSelectedRow(), 9).toString());   
                    txtEmail.setText("" + tablaUsuarios.getValueAt(tablaUsuarios.getSelectedRow(), 10).toString());  
                    txtSueldo.setText(tablaUsuarios.getValueAt(tablaUsuarios.getSelectedRow(), 11).toString());   
                }
            }
    });
        
        //ocultar las contraseñas
        
        PasswordCellRenderer df = new PasswordCellRenderer();
        //df.setBackground(Color.red);
        tablaUsuarios.getColumnModel().getColumn(4).setCellRenderer(df);
}
    
    private void limpiar(){
        txtUsuario.setText("");
        txtNombre.setText("");
        txtApellido.setText("");
        txtClave.setText("");
        txtDireccion.setText("");
        txtTelefono.setText("");
        txtEmail.setText("");
        txtDUI.setText("");
        txtNIT.setText("");
        txtSueldo.setText("");
    }
    
    private void actualizarTabla(){
        List<Object[]> lista;
        lista = Operaciones.getInstancia().consulta("select u.idUsuario,u.nombres,u.apellidos,r.rol,u.clave,u.direccion,u.telefono,u.fecha_ingreso,u.dui,u.nit,u.email,u.sueldo  from usuarios u, roles r where u.idRoles=r.idRoles");
        tablaModelo.setRowCount(0);
        lista.stream().forEach((i)->{tablaModelo.addRow(i);});
        tablaUsuarios.setModel(tablaModelo);
    }
    
    private void insertar() throws NoSuchAlgorithmException{
        
        txtUsuario.setEditable(true);
        Usuarios u= new Usuarios();
        
        if (txtUsuario.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else if (txtNombre.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);        
        }else if (txtApellido.getText().isEmpty()){ 
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);                
        }else if (txtClave.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);                
        }else if (txtDireccion.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE); 
        }else if (txtTelefono.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);        
        }else if (txtEmail.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
       }else if (txtDUI.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
       }else if (txtNIT.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
       }else if (txtSueldo.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);       
       }else {  
            
            u.setIdUsuario(txtUsuario.getText());
            u.setNombres(txtNombre.getText());
            u.setApellidos(txtApellido.getText());
            u.setRoles(lstRol.get(cmbRol.getSelectedIndex()));
            u.setClave(seguridad.sha1(seguridad.sha1(txtClave.getText())));
            u.setDireccion(txtDireccion.getText());
            u.setTelefono(txtTelefono.getText());
            u.setEmail(txtEmail.getText());
            u.setFechaIngreso((java.util.Date)txtFecha.getValue());
            u.setDui(txtDUI.getText());
            u.setNit(txtNIT.getText());
            u.setSueldo(Double.parseDouble(txtSueldo.getText()));
            Operaciones.getInstancia().guardar(u);
            Operaciones.getInstancia().setHabilitado(panelDatos,false);
            limpiar();
        }
        
        actualizarTabla();
        
    }
    
    
    private void modificar() throws NoSuchAlgorithmException{
        txtUsuario.setEditable(false);
        txtUsuario.setEnabled(false);
        Usuarios u= new Usuarios();
        u.setIdUsuario(txtUsuario.getText());
        u.setNombres(txtNombre.getText());
        u.setApellidos(txtApellido.getText());
        u.setRoles(lstRol.get(cmbRol.getSelectedIndex()));
        if(checkClave.isSelected()){
            u.setClave(seguridad.sha1(seguridad.sha1(txtClave.getText())));
        }else{
            u.setClave(txtClave.getText());
        }
        u.setDireccion(txtDireccion.getText());
        u.setTelefono(txtTelefono.getText());
        u.setEmail(txtEmail.getText());
        u.setFechaIngreso((java.util.Date)txtFecha.getValue());
        u.setDui(txtDUI.getText());
        u.setNit(txtNIT.getText());
        u.setSueldo(Double.parseDouble(txtSueldo.getText()));
        
        Operaciones.getInstancia().guardar(u);
        Operaciones.getInstancia().setHabilitado(panelDatos,false);
        limpiar();
        actualizarTabla();
    }
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        panelDatos = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        txtApellido = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtDireccion = new javax.swing.JTextField();
        txtEmail = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtUsuario = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txtFecha = new javax.swing.JSpinner();
        jLabel16 = new javax.swing.JLabel();
        txtSueldo = new javax.swing.JTextField();
        cmbRol = new javax.swing.JComboBox<>();
        txtClave = new javax.swing.JPasswordField();
        txtTelefono = new javax.swing.JFormattedTextField();
        txtDUI = new javax.swing.JFormattedTextField();
        txtNIT = new javax.swing.JFormattedTextField();
        checkClave = new javax.swing.JCheckBox();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaUsuarios = new javax.swing.JTable();
        btnModificar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        txtBuscarUsuario = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Usuarios");

        panelDatos.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos usuario"));

        jLabel3.setText("Usuario:");

        jLabel6.setText("Nombres:");

        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreKeyTyped(evt);
            }
        });

        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        txtApellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtApellidoKeyTyped(evt);
            }
        });

        jLabel7.setText("Apellidos:");

        txtEmail.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtEmailFocusLost(evt);
            }
        });
        txtEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEmailActionPerformed(evt);
            }
        });

        jLabel8.setText("Dirección:");

        jLabel9.setText("Rol:");

        jLabel10.setText("Fecha ingreso:");

        jLabel11.setText("DUI:");

        jLabel12.setText("Email:");

        jLabel13.setText("Sueldo:");

        jLabel14.setText("Clave:");

        jLabel15.setText("Teléfono:");

        txtFecha.setModel(new javax.swing.SpinnerDateModel());

        jLabel16.setText("NIT:");

        txtSueldo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSueldoActionPerformed(evt);
            }
        });
        txtSueldo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtSueldoKeyTyped(evt);
            }
        });

        try {
            txtTelefono.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtTelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTelefonoKeyTyped(evt);
            }
        });

        try {
            txtDUI.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("########-#")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtDUI.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDUIKeyTyped(evt);
            }
        });

        try {
            txtNIT.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####-######-###-#")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtNIT.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNITKeyTyped(evt);
            }
        });

        checkClave.setText("Modificar Clave");

        javax.swing.GroupLayout panelDatosLayout = new javax.swing.GroupLayout(panelDatos);
        panelDatos.setLayout(panelDatosLayout);
        panelDatosLayout.setHorizontalGroup(
            panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDatosLayout.createSequentialGroup()
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDatosLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11))
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(panelDatosLayout.createSequentialGroup()
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDatosLayout.createSequentialGroup()
                                    .addGap(4, 4, 4)
                                    .addComponent(txtDUI, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(panelDatosLayout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panelDatosLayout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panelDatosLayout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelDatosLayout.createSequentialGroup()
                                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel8))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtDireccion, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                                    .addComponent(cmbRol, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(60, 60, 60)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panelDatosLayout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelDatosLayout.createSequentialGroup()
                                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel16)
                                    .addComponent(jLabel15))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtTelefono)
                                    .addComponent(txtNIT, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE))))
                        .addGap(18, 18, 18)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel13)
                            .addComponent(jLabel12)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtApellido, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtEmail, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSueldo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(19, 19, 19)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelDatosLayout.createSequentialGroup()
                                .addComponent(jLabel14)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtClave, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(checkClave))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelDatosLayout.setVerticalGroup(
            panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDatosLayout.createSequentialGroup()
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(cmbRol, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkClave))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtClave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jLabel16)
                    .addComponent(txtSueldo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(txtDUI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNIT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancelar)
                    .addComponent(btnGuardar))
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Listado usuarios"));

        tablaUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Usuario", "Nombres", "Apellido", "Rol", "Clave", "Dirección", "Teléfono", "Fecha ingreso", "DUI", "NIT", "Email", "Sueldo"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaUsuarios.setRowMargin(2);
        jScrollPane1.setViewportView(tablaUsuarios);

        btnModificar.setText("Modificar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnNuevo.setText("Nueva");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(btnNuevo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModificar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEliminar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1123, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(txtBuscarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnBuscar)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBuscarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnModificar)
                    .addComponent(btnEliminar)
                    .addComponent(btnNuevo))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(panelDatos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelDatos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(115, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        if(esModificar){
            
            
            try {
                modificar();
                Log l = new Log();
                    l.setDescripcion("Modificando registros tabla usuarios");
                    l.setOperacion("Modificación");
                    l.setFecha(new java.util.Date());
                    l.setUsuarios((Usuarios)Operaciones.getInstancia().buscar(SingleUser.getInstancia().getIdUsuario(), new Usuarios()));
                    Operaciones.getInstancia().guardar(l,false);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(panelUsuarios.class.getName()).log(Level.SEVERE, null, ex);
            }
            //****************Agregando al log******************************
                    
            //****************Agregando al log******************************
            
        }else{
            
            //****************Agregando al log******************************
                    
            //****************Agregando al log******************************
            try {
                insertar();
                Log l = new Log();
                    l.setDescripcion("Insertando registros tabla usuarios");
                    l.setOperacion("Inserción");
                    l.setFecha(new java.util.Date());
                    l.setUsuarios((Usuarios)Operaciones.getInstancia().buscar(SingleUser.getInstancia().getIdUsuario(), new Usuarios()));
                    Operaciones.getInstancia().guardar(l,false);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(panelUsuarios.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        Operaciones.getInstancia().setHabilitado(panelDatos,false);
        limpiar();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        Operaciones.getInstancia().setHabilitado(panelDatos,true);
        esModificar=false;
        limpiar();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        // TODO add your handling code here:
        esModificar=true;
        Operaciones.getInstancia().setHabilitado(panelDatos,true);
        
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        // TODO add your handling code here:
        if(tablaUsuarios.getRowCount()>0){
            
            //****************Agregando al log******************************
                    Log l = new Log();
                    l.setDescripcion("Eliminando registros tabla usuarios");
                    l.setOperacion("Eliminación");
                    l.setFecha(new java.util.Date());
                    l.setUsuarios((Usuarios)Operaciones.getInstancia().buscar(SingleUser.getInstancia().getIdUsuario(), new Usuarios()));
                    Operaciones.getInstancia().guardar(l,false);
            //****************Agregando al log******************************
            
            Operaciones.getInstancia().eliminar(txtUsuario.getText(),new Usuarios());
            actualizarTabla();
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void txtEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEmailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEmailActionPerformed

    
    
    
        boolean a_entero_valido(String dato){
        try{
            if(dato.toString()!=null){
                return true;
            }else{
                JOptionPane.showMessageDialog(new JFrame(),"Digite letras validas","Datos invalidos",JOptionPane.WARNING_MESSAGE);
                return false;
            }
        }catch(Exception ex){
            JOptionPane.showMessageDialog(new JFrame(),"Digite unicamente letras","Datos invalidos",JOptionPane.WARNING_MESSAGE);
            return false;
        }
    }
    
    int a_entero(String dato){
        try{
            return Integer.parseInt(dato);
        }catch(Exception Ex){
            return -1;
        }
    }
    
    private void txtSueldoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSueldoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSueldoActionPerformed

    
    
    
     /************************VALIDAR QUE CAMPO SEA SOLO LETRA O SOLO NUMEROS*****************************/
    private void txtNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyTyped
        // TODO add your handling code here:
        int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada   
        
         if (k > 47 && k < 58) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar numeros!!!", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtNombre.transferFocus();
        }
    }//GEN-LAST:event_txtNombreKeyTyped

    private void txtApellidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidoKeyTyped
        // TODO add your handling code here:
        int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada   
        
         if (k > 47 && k < 58) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar numeros!!!", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtApellido.transferFocus();
        }
    }//GEN-LAST:event_txtApellidoKeyTyped

    private void txtTelefonoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefonoKeyTyped
        // TODO add your handling code here:
         int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada    
         if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!  Caracteres requeridos (8)", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtTelefono.transferFocus();
        }
    }//GEN-LAST:event_txtTelefonoKeyTyped

    private void txtDUIKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDUIKeyTyped
        // TODO add your handling code here:
         int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada    
         if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!  Caracteres requeridos (9)", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtDUI.transferFocus();
        }
    }//GEN-LAST:event_txtDUIKeyTyped

    private void txtNITKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNITKeyTyped
        // TODO add your handling code here:
         int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada    
         if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!  Caracteres requeridos (14)", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtNIT.transferFocus();
        }
    }//GEN-LAST:event_txtNITKeyTyped

    private void txtSueldoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSueldoKeyTyped
        // TODO add your handling code here:
         int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada    
         if (k >= 97 && k <= 122 || k >= 65 && k <= 90   &&  txtSueldo.getText().contains(".")) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtSueldo.transferFocus();
        }
    }//GEN-LAST:event_txtSueldoKeyTyped

    
    //Metodo para validar email
    public boolean isEmail(String correo){
        Pattern pat = null;
        Matcher mat = null;
        pat = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        mat = pat.matcher(correo);
        if (mat.find()){
            return true;
        }else{
            return false;
        }
    }
    
    private void txtEmailFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtEmailFocusLost
        // TODO add your handling code here:
        if (isEmail(txtEmail.getText())){
            
        }else{
            JOptionPane.showMessageDialog(null, "Email incorrecto", "Validar email", JOptionPane.INFORMATION_MESSAGE);
            txtEmail.requestFocus();
        }
    }//GEN-LAST:event_txtEmailFocusLost

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        // TODO add your handling code here:
        actualizarTabla2(seguridad.consulta_valida(txtBuscarUsuario.getText()));
    }//GEN-LAST:event_btnBuscarActionPerformed
    /************************VALIDAR QUE CAMPO SEA SOLO LETRA O SOLO NUMEROS*****************************/

    private void actualizarTabla2(String nombre){
        List<Object[]> lista;
        lista = Operaciones.getInstancia().consulta("select u.idUsuario,u.nombres,u.apellidos,r.rol,u.clave,u.direccion,u.telefono,u.fecha_ingreso,u.dui,u.nit,u.email,u.sueldo  from usuarios u, roles r where u.idRoles=r.idRoles and u.nombres like '%"+nombre+"%' or u.idUsuario like '%" +nombre+"%' or u.apellidos like '%" + nombre + "%';");
        tablaModelo.setRowCount(0);
        lista.stream().forEach((i)->{tablaModelo.addRow(i);});
        tablaUsuarios.setModel(tablaModelo);
    }

    
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JCheckBox checkClave;
    private javax.swing.JComboBox<String> cmbRol;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panelDatos;
    private javax.swing.JTable tablaUsuarios;
    private javax.swing.JTextField txtApellido;
    private javax.swing.JTextField txtBuscarUsuario;
    private javax.swing.JPasswordField txtClave;
    private javax.swing.JFormattedTextField txtDUI;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JSpinner txtFecha;
    private javax.swing.JFormattedTextField txtNIT;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtSueldo;
    private javax.swing.JFormattedTextField txtTelefono;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables

}
