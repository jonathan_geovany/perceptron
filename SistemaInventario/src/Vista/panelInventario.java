/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//06/2016
package Vista;

import Controlador.Operaciones;
import Controlador.Seguridad;
import Controlador.SingleUser;
import Modelo.Inventarios;
import Modelo.Log;
import Modelo.MarcasProductos;
import Modelo.Roles;
import Modelo.Subcategorias;
import Modelo.Ubicaciones;
import Modelo.Usuarios;
import java.awt.Component;
import java.awt.PopupMenu;
import java.awt.event.KeyEvent;
import java.sql.Date;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jonathan
 */
public class panelInventario extends javax.swing.JPanel {

    /**
     * Creates new form panelSubcategorias
     */
    DefaultTableModel tablaModelo;
    boolean esModificar;
    
    Seguridad s;
    
    List<MarcasProductos> lstMarcas;
    List<Subcategorias> lstSubcategorias;
    List<Ubicaciones> lstUbicaciones;
    public panelInventario() {
        initComponents();
        
        s = new Seguridad();
        
        lstMarcas = (List<MarcasProductos>)(List<?>)Operaciones.getInstancia().lista(new MarcasProductos());
        lstSubcategorias = (List<Subcategorias>)(List<?>)Operaciones.getInstancia().lista(new Subcategorias());
        lstUbicaciones = (List<Ubicaciones>)(List<?>)Operaciones.getInstancia().lista(new Ubicaciones());
        
        for(int i=0;i< lstMarcas.size();i++){
            cmbMarca.addItem(lstMarcas.get(i).getMarca());
        }
        for(int i=0;i<lstSubcategorias.size();i++){
            cmbSubcategoria.addItem(lstSubcategorias.get(i).getNombreSubCategoria());
        }
        for(int i=0;i<lstUbicaciones.size();i++){
            cmbUbicacion.addItem(lstUbicaciones.get(i).getUbicacion());
        }
        
        Operaciones.getInstancia().setHabilitado(panelDatos,false);
        tablaModelo = (DefaultTableModel)tablaInventario.getModel();
        actualizarTabla("");
        tablaInventario.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (tablaInventario.getSelectedRow() > -1) {
                    //actualizar textboxes
                    txtId.setText(tablaInventario.getValueAt(tablaInventario.getSelectedRow(), 0).toString());
                    txtDescripcion.setText(tablaInventario.getValueAt(tablaInventario.getSelectedRow(), 1).toString());
                    txtStockMax.setText(tablaInventario.getValueAt(tablaInventario.getSelectedRow(), 7).toString());   
                    txtStockMin.setText(tablaInventario.getValueAt(tablaInventario.getSelectedRow(), 6).toString());   
                    txtPrecioCom.setText(tablaInventario.getValueAt(tablaInventario.getSelectedRow(), 3).toString()); 
                    txtPrecioVen.setText(tablaInventario.getValueAt(tablaInventario.getSelectedRow(), 4).toString()); 
                    txtDescuento.setText(tablaInventario.getValueAt(tablaInventario.getSelectedRow(), 5).toString());    
                }
            }
    });
    }
    
    private void limpiar(){
        txtId.setText("");
        txtDescripcion.setText("");
        txtStockMax.setText("");
        txtStockMin.setText("");
        txtPrecioCom.setText("");
        txtPrecioVen.setText("");
        txtDescuento.setText("");
    }
    
    private void actualizarTabla(String dato){
        List<Object[]> lista;
        lista = Operaciones.getInstancia().consulta("select i.idInventarios,i.descripcion,i.existencias, i.precio_compra,i.precio_venta,i.descuento,i.stock_minimo,i.stock_maximo,u.ubicacion, s.NombreSubCategoria,m.marca\n" +
"from inventarios i, ubicaciones u, subcategorias s,marcas_productos m \n" +
"where i.idUbicaciones=u.idUbicaciones and i.idSubCategorias = s.idSubcategorias and i.idMarcas_Productos = m.idMarcas_Productos and i.descripcion like '%"+dato+"%'");
        tablaModelo.setRowCount(0);
        lista.stream().forEach((i)->{tablaModelo.addRow(i);});
        tablaInventario.setModel(tablaModelo);
    }
   
    
    private void guardar(){
        Inventarios in= (Inventarios)Operaciones.getInstancia().buscar(Integer.parseInt(txtId.getText()), new Inventarios());
        in.setIdInventarios(Integer.parseInt(txtId.getText()));  
        in.setDescripcion(txtDescripcion.getText());
        in.setStockMaximo(Double.parseDouble(txtStockMax.getText()));
        in.setStockMinimo(Double.parseDouble(txtStockMin.getText()));
        in.setPrecioCompra(Double.parseDouble(txtPrecioCom.getText()));
        in.setPrecioVenta(Double.parseDouble(txtPrecioVen.getText()));
        in.setDescuento(Double.parseDouble(txtDescuento.getText()));
        in.setSubcategorias(lstSubcategorias.get(cmbSubcategoria.getSelectedIndex()));
        in.setUbicaciones(lstUbicaciones.get(cmbUbicacion.getSelectedIndex()));
        in.setMarcasProductos(lstMarcas.get(cmbMarca.getSelectedIndex()));
        //in.setUbicaciones(txtUbicacion.getText());
        //in.setSubcategorias(txtSubCategoria.getText());
        //in.setMarcasProductos(txtMarca.getText());        
        Operaciones.getInstancia().guardar(in);
        Operaciones.getInstancia().setHabilitado(panelDatos,false);
        limpiar();
        actualizarTabla("");
    }
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaInventario = new javax.swing.JTable();
        btnModificar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        txtBuscar = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        panelDatos = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txtDescripcion = new javax.swing.JTextField();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        txtStockMax = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtStockMin = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txtPrecioCom = new javax.swing.JTextField();
        txtPrecioVen = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        txtDescuento = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        cmbUbicacion = new javax.swing.JComboBox<>();
        cmbSubcategoria = new javax.swing.JComboBox<>();
        cmbMarca = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Inventario");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Listado inventario"));

        tablaInventario.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Descripcion", "Existencias", "Precio Compra", "Precio Venta", "Descuento", "Stock minimo", "Stock maximo", "Ubicacion", "Subcategoria", "Marca"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaInventario.setRowMargin(2);
        jScrollPane1.setViewportView(tablaInventario);

        btnModificar.setText("Modificar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(btnModificar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEliminar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1123, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnBuscar)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnModificar)
                    .addComponent(btnEliminar))
                .addContainerGap())
        );

        panelDatos.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos Producto"));

        jLabel6.setText("Descripción:");

        txtDescripcion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDescripcionKeyTyped(evt);
            }
        });

        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        txtStockMax.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtStockMaxKeyTyped(evt);
            }
        });

        jLabel7.setText("Stock máximo:");

        txtStockMin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtStockMinKeyTyped(evt);
            }
        });

        jLabel8.setText("Ubicación:");

        jLabel12.setText("Precio venta:");

        jLabel14.setText("Stock minimo:");

        jLabel15.setText("Precio compra:");

        txtPrecioCom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrecioComActionPerformed(evt);
            }
        });
        txtPrecioCom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrecioComKeyTyped(evt);
            }
        });

        txtPrecioVen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrecioVenActionPerformed(evt);
            }
        });
        txtPrecioVen.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrecioVenKeyTyped(evt);
            }
        });

        jLabel17.setText("Descuento:");

        txtDescuento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDescuentoActionPerformed(evt);
            }
        });
        txtDescuento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDescuentoKeyTyped(evt);
            }
        });

        jLabel18.setText("SubCategoría:");

        jLabel19.setText("Marca:");

        jLabel9.setText("Id:");

        txtId.setEditable(false);

        javax.swing.GroupLayout panelDatosLayout = new javax.swing.GroupLayout(panelDatos);
        panelDatos.setLayout(panelDatosLayout);
        panelDatosLayout.setHorizontalGroup(
            panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDatosLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDatosLayout.createSequentialGroup()
                        .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelDatosLayout.createSequentialGroup()
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel18)
                            .addComponent(jLabel6)
                            .addComponent(jLabel15)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtDescripcion)
                            .addComponent(cmbUbicacion, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbSubcategoria, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtPrecioCom, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                            .addComponent(txtId))
                        .addGap(44, 44, 44)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel12)
                            .addComponent(jLabel17)
                            .addComponent(jLabel19))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelDatosLayout.createSequentialGroup()
                                .addComponent(txtPrecioVen, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel14))
                                .addGap(10, 10, 10)
                                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtStockMax, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtStockMin, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(txtDescuento, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbMarca, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        panelDatosLayout.setVerticalGroup(
            panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDatosLayout.createSequentialGroup()
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDatosLayout.createSequentialGroup()
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(txtStockMax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12)
                            .addComponent(txtPrecioVen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(txtStockMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel17)
                            .addComponent(txtDescuento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cmbMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel19))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(panelDatosLayout.createSequentialGroup()
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtPrecioCom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel15))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cmbUbicacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))
                        .addGap(9, 9, 9)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cmbSubcategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel18))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnCancelar)
                            .addComponent(btnGuardar)))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelDatos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelDatos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        // TODO add your handling code here:
        
        //****************Agregando al log******************************
                    Log l = new Log();
                    l.setDescripcion("Modificando registros tabla inventario");
                    l.setOperacion("Modificación");
                    l.setFecha(new java.util.Date());
                    l.setUsuarios((Usuarios)Operaciones.getInstancia().buscar(SingleUser.getInstancia().getIdUsuario(), new Usuarios()));
                    Operaciones.getInstancia().guardar(l,false);
        //****************Agregando al log******************************
        
        esModificar=true;
        Operaciones.getInstancia().setHabilitado(panelDatos,true);
        
        
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        // TODO add your handling code here:
        if(tablaInventario.getRowCount()>0){
            
            //****************Agregando al log******************************
                    Log l = new Log();
                    l.setDescripcion("Eliminando registros tabla inventario");
                    l.setOperacion("Eliminación");
                    l.setFecha(new java.util.Date());
                    l.setUsuarios((Usuarios)Operaciones.getInstancia().buscar(SingleUser.getInstancia().getIdUsuario(), new Usuarios()));
                    Operaciones.getInstancia().guardar(l,false);
            //****************Agregando al log******************************
        
            Operaciones.getInstancia().eliminar(Integer.parseInt(txtId.getText()),new Inventarios());
            actualizarTabla("");
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        
        if (txtDescripcion.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else if (txtPrecioVen.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else if (txtStockMax.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else if (txtPrecioCom.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else if (txtDescuento.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else if (txtStockMin.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else if (cmbUbicacion.getSelectedItem()==null){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else if (cmbMarca.getSelectedItem()==null){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else if (cmbSubcategoria.getSelectedItem()==null){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else{
        
        //****************Agregando al log******************************
                    Log l = new Log();
                    l.setDescripcion("Insertando registros tabla inventario");
                    l.setOperacion("Inserción");
                    l.setFecha(new java.util.Date());
                    l.setUsuarios((Usuarios)Operaciones.getInstancia().buscar(SingleUser.getInstancia().getIdUsuario(), new Usuarios()));
                    Operaciones.getInstancia().guardar(l,false);
        //****************Agregando al log******************************
        
        guardar();
        
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        Operaciones.getInstancia().setHabilitado(panelDatos,false);
        
        limpiar();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtPrecioComActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrecioComActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPrecioComActionPerformed

    private void txtPrecioVenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrecioVenActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPrecioVenActionPerformed

    private void txtDescuentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDescuentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDescuentoActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        // TODO add your handling code here:
        actualizarTabla(s.consulta_valida(txtBuscar.getText()));
    }//GEN-LAST:event_btnBuscarActionPerformed

    
    
    
    private void txtDescripcionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDescripcionKeyTyped
        // TODO add your handling code here:
        int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada   
        
         if (k > 47 && k < 58) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar numeros!!!", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtDescripcion.transferFocus();
        }
    }//GEN-LAST:event_txtDescripcionKeyTyped

    private void txtPrecioVenKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecioVenKeyTyped
        // TODO add your handling code here:
        int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada    
         if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!  Caracteres requeridos (8)", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtPrecioVen.transferFocus();
        }
    }//GEN-LAST:event_txtPrecioVenKeyTyped

    private void txtStockMaxKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStockMaxKeyTyped
        // TODO add your handling code here:
        int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada    
         if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!  Caracteres requeridos (8)", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtStockMax.transferFocus();
        }
    }//GEN-LAST:event_txtStockMaxKeyTyped

    private void txtPrecioComKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecioComKeyTyped
        // TODO add your handling code here:
        int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada    
         if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!  Caracteres requeridos (8)", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtPrecioCom.transferFocus();
        }
    }//GEN-LAST:event_txtPrecioComKeyTyped

    private void txtDescuentoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDescuentoKeyTyped
        // TODO add your handling code here:
        int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada    
         if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!  Caracteres requeridos (8)", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtDescuento.transferFocus();
        }
    }//GEN-LAST:event_txtDescuentoKeyTyped

    private void txtStockMinKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStockMinKeyTyped
        // TODO add your handling code here:
        int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada    
         if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!  Caracteres requeridos (8)", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtStockMin.transferFocus();
        }
    }//GEN-LAST:event_txtStockMinKeyTyped

    
    
    
    
    
    
    
        boolean a_entero_valido(String dato){
        try{
            if(dato.toString()!=null){
                return true;
            }else{
                JOptionPane.showMessageDialog(new JFrame(),"Digite letras validas","Datos invalidos",JOptionPane.WARNING_MESSAGE);
                return false;
            }
        }catch(Exception ex){
            JOptionPane.showMessageDialog(new JFrame(),"Digite unicamente letras","Datos invalidos",JOptionPane.WARNING_MESSAGE);
            return false;
        }
    }
    
    int a_entero(String dato){
        try{
            return Integer.parseInt(dato);
        }catch(Exception Ex){
            return -1;
        }
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JComboBox<String> cmbMarca;
    private javax.swing.JComboBox<String> cmbSubcategoria;
    private javax.swing.JComboBox<String> cmbUbicacion;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panelDatos;
    private javax.swing.JTable tablaInventario;
    private javax.swing.JTextField txtBuscar;
    private javax.swing.JTextField txtDescripcion;
    private javax.swing.JTextField txtDescuento;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtPrecioCom;
    private javax.swing.JTextField txtPrecioVen;
    private javax.swing.JTextField txtStockMax;
    private javax.swing.JTextField txtStockMin;
    // End of variables declaration//GEN-END:variables

}
