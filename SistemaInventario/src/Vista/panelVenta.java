/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.*;
import Modelo.*;
import Reportes.rptFacturaCompra;
import Reportes.rptFacturaVenta;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import org.hibernate.Session;
/**
 *
 * @author Jonathan
 */
public class panelVenta extends javax.swing.JPanel {

    /**
     * Creates new form panelVenta
     */
    private Clientes cliente;
    private Set detallesVenta;
    private TableCellListener listenerTabla;
    private int filas = 1;
    public panelVenta() {
        initComponents();
        
      
        
        detallesVenta = new HashSet(0);
        Action action = new AbstractAction()
        {
            public void actionPerformed(ActionEvent e)
            {
                TableCellListener tcl = (TableCellListener)e.getSource();
                //System.out.println("Row   : " + tcl.getRow());
                //System.out.println("Column: " + tcl.getColumn());
                //System.out.println("Old   : " + tcl.getOldValue());
                //System.out.println("New   : " + tcl.getNewValue());
                if(tcl.getColumn()==0 ){
                    agregarDetalle(tcl.getNewValue(),tcl.getRow());
                    if(tcl.getOldValue()==null){
                        agregarFila();
                    }
                }else{
                    actualizarDetalle(tcl.getRow());
                }
                actualizarSumas();
            }
        };
        
        listenerTabla = new TableCellListener(this.tablaDetalles,action);
    }
    
    void agregarFila(){
        DefaultTableModel  modelo = (DefaultTableModel) this.tablaDetalles.getModel();
        filas++;
        modelo.setRowCount(filas);
    }
    
    void agregarDetalle(Object pk,Integer fila){
        int llave = a_entero(pk.toString());
        Inventarios prod = (Inventarios)Operaciones.getInstancia().buscar(llave, new Inventarios());
        if(prod!=null){
            this.tablaDetalles.getModel().setValueAt(prod.getDescripcion(), fila, 1);
            this.tablaDetalles.getModel().setValueAt(prod.getPrecioVenta(), fila, 2);
            this.tablaDetalles.getModel().setValueAt(1, fila, 3);
            this.tablaDetalles.getModel().setValueAt(prod.getDescuento(), fila, 4);
            this.tablaDetalles.getModel().setValueAt(prod.getExistencias(), fila, 5);
            this.tablaDetalles.getModel().setValueAt(0, fila, 6);
            this.tablaDetalles.getModel().setValueAt(0, fila, 7);
            this.tablaDetalles.getModel().setValueAt(prod.getPrecioVenta(), fila, 8);
            
        }else{
            JOptionPane.showMessageDialog(null, "No se encontro el producto ");
            this.tablaDetalles.getModel().setValueAt("", fila, 0);
            this.tablaDetalles.getModel().setValueAt("", fila, 1);
            this.tablaDetalles.getModel().setValueAt("", fila, 2);
            this.tablaDetalles.getModel().setValueAt("", fila, 3);
            this.tablaDetalles.getModel().setValueAt("", fila, 4);
            this.tablaDetalles.getModel().setValueAt("", fila, 5);
            this.tablaDetalles.getModel().setValueAt("", fila, 6);
            this.tablaDetalles.getModel().setValueAt("", fila, 7);
            this.tablaDetalles.getModel().setValueAt("", fila, 8);
        }
        actualizarSumas();
    }
    
    void actualizarDetalle(Integer fila){
        int cantidad = a_entero(this.tablaDetalles.getModel().getValueAt(fila, 3).toString());
        Double descuento = Double.parseDouble(this.tablaDetalles.getModel().getValueAt(fila, 4).toString());
        Double existencias = Double.parseDouble(this.tablaDetalles.getModel().getValueAt(fila, 5).toString());
        Double Precio = Double.parseDouble(this.tablaDetalles.getModel().getValueAt(fila, 2).toString());
        if(cantidad>0 && cantidad<=existencias){
            Double total = (cantidad - (cantidad*descuento))*Precio;
            this.tablaDetalles.getModel().setValueAt(total, fila, 8);
        }else{
            JOptionPane.showMessageDialog(null, "Dato no valido");
            this.tablaDetalles.getModel().setValueAt(1, fila, 3);
            this.tablaDetalles.getModel().setValueAt(Precio, fila, 8);
        }
    }
    
    void actualizarSumas(){
        Double v_ex=0.0;
        Double v_no=0.0;
        Double v_af=0.0;
        
        for(int i=0;i<tablaDetalles.getRowCount()-1;i++){
            int id = a_entero(this.tablaDetalles.getModel().getValueAt(i, 0).toString());
            if(id>0){
                v_ex += Double.parseDouble(this.tablaDetalles.getModel().getValueAt(i, 6).toString());
                v_no += Double.parseDouble(this.tablaDetalles.getModel().getValueAt(i, 7).toString());
                v_af += Double.parseDouble(this.tablaDetalles.getModel().getValueAt(i, 8).toString());
            }
        }
        txtSubtotal.setText(v_af.toString());
        txtTotalPagar.setText(v_af.toString());
        txtIVA.setText("0.13");
        txtIVAPercibido.setText("0");
        txtIVARetenido.setText("0");
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txtFecha = new javax.swing.JSpinner();
        jLabel6 = new javax.swing.JLabel();
        txtCliente = new javax.swing.JTextField();
        btnBuscarCliente = new javax.swing.JButton();
        btnSeleccionarCliente = new javax.swing.JButton();
        txtNombresCliente = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtDireccionCliente = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtTelefonoCliente = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaDetalles = new javax.swing.JTable();
        btnEliminarDetalle = new javax.swing.JButton();
        btnBuscarProd = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        txtIVA = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtSubtotal = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txtIVARetenido = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txtIVAPercibido = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txtTotalPagar = new javax.swing.JTextField();
        jButton7 = new javax.swing.JButton();

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos Factura"));

        jLabel5.setText("Fecha : ");

        txtFecha.setModel(new javax.swing.SpinnerDateModel());

        jLabel6.setText("Cliente id : ");

        txtCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtClienteKeyTyped(evt);
            }
        });

        btnBuscarCliente.setText("Buscar");
        btnBuscarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarClienteActionPerformed(evt);
            }
        });

        btnSeleccionarCliente.setText("Seleccionar");
        btnSeleccionarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeleccionarClienteActionPerformed(evt);
            }
        });

        txtNombresCliente.setEditable(false);
        txtNombresCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombresClienteKeyTyped(evt);
            }
        });

        jLabel8.setText("Dirección : ");

        txtDireccionCliente.setEditable(false);
        txtDireccionCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDireccionClienteKeyTyped(evt);
            }
        });

        jLabel9.setText("Telefono : ");

        txtTelefonoCliente.setEditable(false);
        txtTelefonoCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTelefonoClienteKeyTyped(evt);
            }
        });

        jLabel7.setText("Nombres: ");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBuscarCliente)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSeleccionarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtNombresCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtTelefonoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDireccionCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(btnBuscarCliente)
                    .addComponent(btnSeleccionarCliente)
                    .addComponent(txtCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNombresCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(txtTelefonoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(txtDireccionCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(98, 98, 98))
        );

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Nueva Venta");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Detalles"));

        tablaDetalles.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Código", "Descripción", "P. Unitario", "Cantidad", "Descuento", "Existencias", "V. Exenta", "V. No sujetas", "Ventas Afectas"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, false, true, true, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaDetalles.setRowMargin(2);
        jScrollPane1.setViewportView(tablaDetalles);
        if (tablaDetalles.getColumnModel().getColumnCount() > 0) {
            tablaDetalles.getColumnModel().getColumn(0).setPreferredWidth(6);
            tablaDetalles.getColumnModel().getColumn(1).setPreferredWidth(20);
            tablaDetalles.getColumnModel().getColumn(2).setPreferredWidth(7);
            tablaDetalles.getColumnModel().getColumn(6).setPreferredWidth(7);
            tablaDetalles.getColumnModel().getColumn(7).setPreferredWidth(7);
            tablaDetalles.getColumnModel().getColumn(8).setPreferredWidth(10);
        }

        btnEliminarDetalle.setText("Eliminar");
        btnEliminarDetalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarDetalleActionPerformed(evt);
            }
        });

        btnBuscarProd.setText("Buscar Producto");
        btnBuscarProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarProdActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(btnBuscarProd)
                .addContainerGap(1119, Short.MAX_VALUE))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1230, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(btnEliminarDetalle)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(btnBuscarProd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnEliminarDetalle))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos Contables"));

        jLabel17.setText("IVA : ");

        txtIVA.setEditable(false);
        txtIVA.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtIVAKeyTyped(evt);
            }
        });

        jLabel18.setText("Subtotal : ");

        txtSubtotal.setEditable(false);

        jLabel19.setText("IVA Retenido : ");

        txtIVARetenido.setEditable(false);
        txtIVARetenido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtIVARetenidoKeyTyped(evt);
            }
        });

        jLabel20.setText("IVA Percibido : ");

        txtIVAPercibido.setEditable(false);
        txtIVAPercibido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtIVAPercibidoKeyTyped(evt);
            }
        });

        jLabel21.setText("Total a pagar : ");

        txtTotalPagar.setEditable(false);

        jButton7.setText("Procesar Venta");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel21)
                    .addComponent(jLabel18))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtSubtotal)
                    .addComponent(txtTotalPagar, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jButton7, javax.swing.GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE)
                        .addGap(787, 787, 787))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtIVA, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel19)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtIVARetenido, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtIVAPercibido, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(txtSubtotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17)
                    .addComponent(txtIVA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19)
                    .addComponent(txtIVARetenido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20)
                    .addComponent(txtIVAPercibido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(txtTotalPagar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton7))
                .addGap(0, 13, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void limpiarCliente(){
        this.txtNombresCliente.setText("");
        this.txtTelefonoCliente.setText("");
        this.txtDireccionCliente.setText("");
    }
    
    boolean a_entero_valido(String dato){
        try{
            if(Integer.parseInt(dato)>0){
                return true;
            }else{
                JOptionPane.showMessageDialog(new JFrame(),"Digite números validos","Datos invalidos",JOptionPane.WARNING_MESSAGE);
                return false;
            }
        }catch(Exception ex){
            JOptionPane.showMessageDialog(new JFrame(),"Digite unicamente números","Datos invalidos",JOptionPane.WARNING_MESSAGE);
            return false;
        }
    }
    
    int a_entero(String dato){
        try{
            return Integer.parseInt(dato);
        }catch(Exception Ex){
            return -1;
        }
    }
    
    private void guardarFactura(){
        
        if(valido()){
            
            
            Ventas factura = new Ventas();
            factura.setClientes((Clientes) Operaciones.getInstancia().buscar( a_entero(txtCliente.getText()) , new Clientes()) );
            factura.setFecha((Date)txtFecha.getValue() );
            factura.setUsuarios((Usuarios)Operaciones.getInstancia().buscar(SingleUser.getInstancia().getIdUsuario(), new Usuarios()));
            List<Object[]> datosTiraje;
            int idTiraje;
            Tiraje t;
            try{
                datosTiraje= Operaciones.getInstancia().consulta("select * from tiraje where estado like '%activo%' and tipo like '%factura contado%'");
                idTiraje = (int)datosTiraje.get(0)[0];
                t = (Tiraje)Operaciones.getInstancia().buscar(idTiraje, new Tiraje());
            }catch(Exception ex5){
                JOptionPane.showMessageDialog(null, "Seleccione un tiraje activo tipo factura contado para procesar la venta");
                return;
            }
            
            
            factura.setTiraje(t);
            factura.setCorrelativo(""+t.getCorrelativo());
            factura.setEstatus("retenida");
            factura.setCondicionPago("contado");
            factura.setVentasExentas(0.0);
            factura.setVentasNoSujetas(0.0);
            factura.setSubTotal(Double.parseDouble(txtSubtotal.getText()));
            Double decimalTotal = Double.parseDouble(txtTotalPagar.getText());
            Integer enteroTotal = decimalTotal.intValue();
            factura.setTotal(decimalTotal);
            factura.setIvaRetenido(0.0);
            n2t a_cantidad = new n2t();
            String cantidad  = a_cantidad.convertirLetras(enteroTotal);
            Double residuo = (decimalTotal-enteroTotal);
            cantidad += " con "+residuo.toString() + " dolares";
            factura.setCantidadEnLetras(cantidad);
            if(t.getCorrelativo()> t.getNumFin() ){
                JOptionPane.showMessageDialog(null, "Tiraje lleno, cambie el estado a desactivado y eliga otro nuevo tiraje de factura contado");
                return;//termino la ejecucion de guardar
            }
            
            if(Operaciones.getInstancia().guardar(factura,true)){
                //si se guardo ok
                t.setCorrelativo(t.getCorrelativo()+1);
                Operaciones.getInstancia().guardar(t,false);
            
                for(int i=0;i<tablaDetalles.getRowCount()-1;i++){
                    int id = a_entero(this.tablaDetalles.getModel().getValueAt(i, 0).toString());
                    if(id>0){
                        DetallesVentas detalle = new DetallesVentas();
                        detalle.setInventarios((Inventarios)Operaciones.getInstancia().buscar(id, new Inventarios()));
                        detalle.setVentas(factura);
                        detalle.setCantidad(Double.parseDouble(this.tablaDetalles.getModel().getValueAt(i, 3).toString()));
                        detalle.setPrecioUnitario(Double.parseDouble(this.tablaDetalles.getModel().getValueAt(i, 2).toString()));
                        detalle.setDescripcion(this.tablaDetalles.getModel().getValueAt(i, 1).toString());
                        detalle.setVentasExentas(Double.parseDouble(this.tablaDetalles.getModel().getValueAt(i, 6).toString()));
                        detalle.setVentasNoSujetas(Double.parseDouble(this.tablaDetalles.getModel().getValueAt(i, 7).toString()));
                        detalle.setVentasAfectadas(Double.parseDouble(this.tablaDetalles.getModel().getValueAt(i, 8).toString()));
                        Operaciones.getInstancia().guardar(detalle,false);
                    }
                }
                int reply = JOptionPane.showConfirmDialog(null, "¿Desea imprimir la factura?", "Informacion", JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION) {
                    rptFacturaVenta rpt = new rptFacturaVenta();
                    rpt.id = factura.getIdVentas();
                    rpt.reporteMostrar();
                }
                JTabbedPane padre = ((JTabbedPane)SwingUtilities.getAncestorOfClass(JTabbedPane.class, panelVenta.this));
                int selected = padre.getSelectedIndex();
                padre.remove(selected);
            }else{
                
                JOptionPane.showMessageDialog(null, "No se pudo procesar la factura" );
            }
        }else{
            JOptionPane.showMessageDialog(null, "Datos incompletos");
        }
    }
    
    private boolean valido(){
        if(txtCliente.getText().equals("") || txtTotalPagar.getText().equals("") || txtTotalPagar.getText().equals("0.0")){
            return false;
        }
        return true;
    }
    
    private void btnBuscarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarClienteActionPerformed
        // TODO add your handling code here:
        
        if(this.txtCliente.getText().length()>0 && a_entero_valido(this.txtCliente.getText())){
            cliente = (Clientes)Operaciones.getInstancia().buscar(Integer.parseInt(this.txtCliente.getText()), new Clientes());
            if(cliente!=null){
                this.txtNombresCliente.setText(cliente.getNombres());
                this.txtDireccionCliente.setText(cliente.getDireccion());
                this.txtTelefonoCliente.setText(cliente.getTelefono());
            }else{
                JOptionPane.showMessageDialog(null, "No se encontro el cliente ");
            }
        }else{
            this.limpiarCliente();
        }
        
    }//GEN-LAST:event_btnBuscarClienteActionPerformed

    private void btnBuscarProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarProdActionPerformed
        // TODO add your handling code here:
        dialogoBuscarInventario dialogo = new dialogoBuscarInventario(new Frame(), true);
        dialogo.setVisible(true);
        Inventarios c = dialogo.c;
        if(c!=null){
            agregarFila();
            tablaDetalles.getModel().setValueAt(c.getIdInventarios(),filas-2, 0);
            agregarDetalle(c.getIdInventarios(),filas-2);
        }
    }//GEN-LAST:event_btnBuscarProdActionPerformed

    private void btnEliminarDetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarDetalleActionPerformed
        // TODO add your handling code here:
        
        //****************Agregando al log******************************
                    Log l = new Log();
                    l.setDescripcion("Eliminando registros tabla ventas");
                    l.setOperacion("Eliminación");
                    l.setFecha(new Date());
                    l.setUsuarios((Usuarios)Operaciones.getInstancia().buscar(SingleUser.getInstancia().getIdUsuario(), new Usuarios()));
                    Operaciones.getInstancia().guardar(l,false);
        //****************Agregando al log******************************
            
        
        int selectedRow = tablaDetalles.getSelectedRow();
        
        DefaultTableModel model = (DefaultTableModel) this.tablaDetalles.getModel();
        if(model.getRowCount() >  1 && selectedRow!=-1) {
            if(selectedRow!=model.getRowCount()-1){
                model.removeRow(selectedRow);
                filas--;
            }
        }
        actualizarSumas();
    }//GEN-LAST:event_btnEliminarDetalleActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        if (txtCliente.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else if(txtNombresCliente.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else if (txtTelefonoCliente.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else if(txtDireccionCliente.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else if(txtIVA.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else if(txtIVARetenido.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else if(txtIVAPercibido.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No se permiten campos vacios", "error", JOptionPane.ERROR_MESSAGE);
        }else{
        
        try{
            
            //****************Agregando al log******************************
                    Log l = new Log();
                    l.setDescripcion("Insertando registros tabla ventas");
                    l.setOperacion("Inserción");
                    l.setFecha(new Date());
                    l.setUsuarios((Usuarios)Operaciones.getInstancia().buscar(SingleUser.getInstancia().getIdUsuario(), new Usuarios()));
                    Operaciones.getInstancia().guardar(l,false);
            //****************Agregando al log******************************
            
            guardarFactura();
            
        }catch(Exception ex4){
            JOptionPane.showMessageDialog(null, "No se pudo guardar");
        }
        
        }
    }//GEN-LAST:event_jButton7ActionPerformed

    private void btnSeleccionarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeleccionarClienteActionPerformed
        // TODO add your handling code here:
        dialogoBuscarCliente dialogo = new dialogoBuscarCliente(new Frame(), true);
        dialogo.setVisible(true);
        Clientes c = dialogo.c;
        if(c!=null){
            txtCliente.setText(""+c.getIdClientes());
            txtNombresCliente.setText(c.getNombres());
            txtTelefonoCliente.setText(c.getTelefono());
            txtDireccionCliente.setText(c.getDireccion());
        }
        
        
    }//GEN-LAST:event_btnSeleccionarClienteActionPerformed

    
    
    
    private void txtClienteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtClienteKeyTyped
        // TODO add your handling code here:
        int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada    
         if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!  Caracteres requeridos (8)", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
        if (k == 10) {
           //transfiere el foco si presionas enter
            txtCliente.transferFocus();
        }
    }//GEN-LAST:event_txtClienteKeyTyped

    private void txtNombresClienteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombresClienteKeyTyped
        // TODO add your handling code here:
        int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada   
        
         if (k > 47 && k < 58) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar numeros!!!", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtNombresCliente.transferFocus();
        }
    }//GEN-LAST:event_txtNombresClienteKeyTyped

    private void txtTelefonoClienteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefonoClienteKeyTyped
        // TODO add your handling code here:
        int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada    
         if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!  Caracteres requeridos (8)", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtTelefonoCliente.transferFocus();
        }
    }//GEN-LAST:event_txtTelefonoClienteKeyTyped

    private void txtDireccionClienteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDireccionClienteKeyTyped
        // TODO add your handling code here:
        
    }//GEN-LAST:event_txtDireccionClienteKeyTyped

    private void txtIVAKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIVAKeyTyped
        // TODO add your handling code here:
        int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada    
         if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!  Caracteres requeridos (8)", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtIVA.transferFocus();
        }
    }//GEN-LAST:event_txtIVAKeyTyped

    private void txtIVARetenidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIVARetenidoKeyTyped
        // TODO add your handling code here:
        int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada    
         if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!  Caracteres requeridos (8)", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtIVARetenido.transferFocus();
        }
    }//GEN-LAST:event_txtIVARetenidoKeyTyped

    private void txtIVAPercibidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIVAPercibidoKeyTyped
        // TODO add your handling code here:
        int k = (int) evt.getKeyChar();//k = al valor de la tecla presionada    
         if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {//Si el caracter ingresado es una letra
             evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
             JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!  Caracteres requeridos (8)", "Validando Datos",
                     JOptionPane.ERROR_MESSAGE);
         }
         if (k == 10) {
           //transfiere el foco si presionas enter
            txtIVAPercibido.transferFocus();
        }
    }//GEN-LAST:event_txtIVAPercibidoKeyTyped

    
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarCliente;
    private javax.swing.JButton btnBuscarProd;
    private javax.swing.JButton btnEliminarDetalle;
    private javax.swing.JButton btnSeleccionarCliente;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaDetalles;
    private javax.swing.JTextField txtCliente;
    private javax.swing.JTextField txtDireccionCliente;
    private javax.swing.JSpinner txtFecha;
    private javax.swing.JTextField txtIVA;
    private javax.swing.JTextField txtIVAPercibido;
    private javax.swing.JTextField txtIVARetenido;
    private javax.swing.JTextField txtNombresCliente;
    private javax.swing.JTextField txtSubtotal;
    private javax.swing.JTextField txtTelefonoCliente;
    private javax.swing.JTextField txtTotalPagar;
    // End of variables declaration//GEN-END:variables
}
