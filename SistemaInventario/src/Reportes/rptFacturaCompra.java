/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reportes;

import Controlador.Operaciones;
import Vista.frmPrincipal;
import java.io.File;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.view.JasperViewer;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JRViewer;

/**
 *
 * @author Claudia_94
 */
public class rptFacturaCompra {
     
    public int id;
      
    private JasperPrint informe;
    private JasperReport reporte;
    private JasperPrint imprime;
    private JasperViewer ventanavisor;
    private JRViewer mostrar;
    
    //JDialog viewer = new JDialog(new frmPrincipal(),"reporte",true);
    private Connection conn;
    
    /*public void reporteCrear(){
        //Connection conn;
        
        try {
              String ruta = "Reportes\\rptFacturaVenta.jasper";
              Map parametros = new HashMap();
              //parametros.put("codigo", txtId.getText());
              
              //informe = JasperFillManager.fillReport(inputStream, parameters, connection);
              
              //reporte = (JasperReport) JRLoader.loadObjectFromFile(ruta);
              //imprime = JasperFillManager.fillReport(reporte, null);
              imprime = JasperFillManager.fillReport(ruta, null);
              
        } catch (JRException ex) {
            Logger.getLogger(rptFacturaVenta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void reporteMostrar(){
            
        ventanavisor = new JasperViewer(imprime);
        ventanavisor.setTitle("Factura venta");
        ventanavisor.setVisible(true);
             
    }*/
    
    public void reporteMostrar(){
        conn = Operaciones.getInstancia().getConexion();
        Map parametro = new HashMap();
        parametro.put("id_factura", id);
        try{
            File f = new File("src\\Reportes\\rptFacturaCompra.jasper");
            //reporte = JasperCompileManager.compileReport(f.getAbsolutePath());
            //reporte = JasperCompileManager.compileReport("rptFacturaVenta.jrxml");
            reporte = (JasperReport) JRLoader.loadObjectFromFile(f.getAbsolutePath());
            imprime = JasperFillManager.fillReport(reporte, parametro,conn);
            ventanavisor = new JasperViewer(imprime,false);
            ventanavisor.setTitle("Factura Compra");
            ventanavisor.setVisible(true);
        } catch (JRException ex) {
            Logger.getLogger(rptFacturaCompra.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
}