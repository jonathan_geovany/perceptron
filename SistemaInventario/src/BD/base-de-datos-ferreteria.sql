-- MySQL Workbench Forward Engineering


SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

SET SQL_SAFE_UPDATES = 0;

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema sistemaferreteria
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema sistemaferreteria
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `sistemaferreteria` DEFAULT CHARACTER SET utf8 ;
USE `sistemaferreteria` ;

-- -----------------------------------------------------
-- Table `sistemaferreteria`.`categorias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`categorias` (
  `idCategorias` INT(11) NULL AUTO_INCREMENT,
  `NombreCategoria` VARCHAR(60) NOT NULL,
  `TotalProducto` DOUBLE NULL,
  PRIMARY KEY (`idCategorias`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaferreteria`.`subcategorias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`subcategorias` (
  `idSubcategorias` INT(11) NULL AUTO_INCREMENT,
  `NombreSubCategoria` VARCHAR(60) NOT NULL,
  `TotalProducto` DOUBLE NULL,
  `idCategoria` INT NOT NULL,
  PRIMARY KEY (`idSubcategorias`),
  INDEX `fk_IdCategoria` (`idCategoria` ASC),
  CONSTRAINT `fk_IdCategoria`
    FOREIGN KEY (`idCategoria`)
    REFERENCES `sistemaferreteria`.`categorias` (`idCategorias`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaferreteria`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`roles` (
  `idRoles` INT(11) NULL AUTO_INCREMENT,
  `rol` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`idRoles`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaferreteria`.`usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`usuarios` (
  `idUsuario` VARCHAR(45) NULL,
  `idRoles` INT(11) NOT NULL,
  `nombres` VARCHAR(100) NULL DEFAULT NULL,
  `apellidos` VARCHAR(100) NULL DEFAULT NULL,
  `clave` VARCHAR(60) NULL DEFAULT NULL,
  `direccion` VARCHAR(100) NULL,
  `telefono` VARCHAR(15) NULL DEFAULT NULL,
  `email` VARCHAR(60) NULL,
  `fecha_ingreso` DATE NULL DEFAULT NULL,
  `dui` VARCHAR(45) NULL DEFAULT NULL,
  `nit` VARCHAR(45) NULL DEFAULT NULL,
  `sueldo` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`idUsuario`),
  INDEX `fk_Roles1` (`idRoles` ASC),
  CONSTRAINT `fk_Roles1`
    FOREIGN KEY (`idRoles`)
    REFERENCES `sistemaferreteria`.`roles` (`idRoles`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaferreteria`.`clientes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`clientes` (
  `idClientes` INT(11) NULL AUTO_INCREMENT,
  `idUsuario` VARCHAR(45) NOT NULL,
  `nombres` VARCHAR(100) NULL DEFAULT NULL,
  `apellidos` VARCHAR(100) NULL DEFAULT NULL,
  `direccion` VARCHAR(200) NULL DEFAULT NULL,
  `telefono` VARCHAR(15) NULL DEFAULT NULL,
  `dui` VARCHAR(15) NULL DEFAULT NULL,
  `nit` VARCHAR(15) NULL DEFAULT NULL,
  `nrc` VARCHAR(15) NULL DEFAULT NULL,
  PRIMARY KEY (`idClientes`),
  INDEX `fk_Usuarios2` (`idUsuario` ASC),
  CONSTRAINT `fk_Usuarios2`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `sistemaferreteria`.`usuarios` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaferreteria`.`proveedores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`proveedores` (
  `idProveedores` INT(11) NULL AUTO_INCREMENT,
  `nombres` VARCHAR(100) NULL DEFAULT NULL,
  `apellidos` VARCHAR(100) NULL DEFAULT NULL,
  `direccion` VARCHAR(200) NULL DEFAULT NULL,
  `DUI` VARCHAR(20) NULL DEFAULT NULL,
  `NIT` VARCHAR(20) NULL DEFAULT NULL,
  `telefono` VARCHAR(45) NULL DEFAULT NULL,
  `email` VARCHAR(45) NULL,
  `NRC` VARCHAR(45) NULL,
  PRIMARY KEY (`idProveedores`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaferreteria`.`compras`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`compras` (
  `idCompras` INT(11) NULL AUTO_INCREMENT,
  `idUsuario` VARCHAR(45) NOT NULL,
  `idProveedores` INT(11) NOT NULL,
  `correlativo` VARCHAR(100) NULL,
  `fecha` DATETIME NULL,
  `condicion_pago` VARCHAR(45) NULL,
  `estatus` VARCHAR(45) NULL,
  `iva` DOUBLE NULL,
  `iva_percibido` DOUBLE NULL,
  `iva_retenido` DOUBLE NULL,
  `subTotal` DOUBLE NULL,
  `TOTAL` DOUBLE NULL,
  PRIMARY KEY (`idCompras`),
  INDEX `fk_Usuarios7` (`idUsuario` ASC),
  INDEX `fk_Proveedores1` (`idProveedores` ASC),
  CONSTRAINT `fk_Proveedores1`
    FOREIGN KEY (`idProveedores`)
    REFERENCES `sistemaferreteria`.`proveedores` (`idProveedores`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuarios7`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `sistemaferreteria`.`usuarios` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaferreteria`.`marcas_productos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`marcas_productos` (
  `idMarcas_Productos` INT(11) NULL AUTO_INCREMENT,
  `marca` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`idMarcas_Productos`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaferreteria`.`ubicaciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`ubicaciones` (
  `idUbicaciones` INT(11) NULL AUTO_INCREMENT,
  `ubicacion` VARCHAR(55) NULL DEFAULT NULL,
  PRIMARY KEY (`idUbicaciones`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaferreteria`.`inventarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`inventarios` (
  `idInventarios` INT(11) NULL AUTO_INCREMENT,
  `idUbicaciones` INT(11) NOT NULL,
  `idSubCategorias` INT(11) NOT NULL,
  `idMarcas_Productos` INT(11) NOT NULL,
  `descripcion` VARCHAR(200) NULL DEFAULT NULL,
  `stock_maximo` DOUBLE NULL DEFAULT NULL,
  `stock_minimo` DOUBLE NULL DEFAULT NULL,
  `existencias` DOUBLE NULL DEFAULT NULL,
  `precio_compra` DOUBLE NULL DEFAULT NULL,
  `precio_venta` DOUBLE NULL DEFAULT NULL,
  `descuento` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`idInventarios`),
  INDEX `fk_Ubicaciones1` (`idUbicaciones` ASC),
  INDEX `fk_Categorias3` (`idSubCategorias` ASC),
  INDEX `fk_Marcas_Productos4` (`idMarcas_Productos` ASC),
  CONSTRAINT `fk_Categorias3`
    FOREIGN KEY (`idSubCategorias`)
    REFERENCES `sistemaferreteria`.`subcategorias` (`idSubcategorias`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Marcas_Productos4`
    FOREIGN KEY (`idMarcas_Productos`)
    REFERENCES `sistemaferreteria`.`marcas_productos` (`idMarcas_Productos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ubicaciones1`
    FOREIGN KEY (`idUbicaciones`)
    REFERENCES `sistemaferreteria`.`ubicaciones` (`idUbicaciones`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaferreteria`.`detalles_compras`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`detalles_compras` (
  `idFactura` INT(11) NULL AUTO_INCREMENT,
  `idCompras` INT(11) NOT NULL,
  `idInventario` INT(11) NOT NULL,
  `cantidad` DOUBLE NULL DEFAULT NULL,
  `descripcion` VARCHAR(105) NULL DEFAULT NULL,
  `precioUnitario` DOUBLE NULL DEFAULT NULL,
  `ventas_exentas` DOUBLE NULL DEFAULT NULL,
  `ventas_gravadas` DOUBLE NULL DEFAULT NULL,
  `ventasNoSujetas` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`idFactura`),
  INDEX `fk_Compras1` (`idCompras` ASC),
  INDEX `fk_Inventarios2` (`idInventario` ASC),
  CONSTRAINT `fk_Compras1`
    FOREIGN KEY (`idCompras`)
    REFERENCES `sistemaferreteria`.`compras` (`idCompras`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Inventarios2`
    FOREIGN KEY (`idInventario`)
    REFERENCES `sistemaferreteria`.`inventarios` (`idInventarios`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaferreteria`.`ventas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`ventas` (
  `idVentas` INT(11) NULL AUTO_INCREMENT,
  `idClientes` INT(11) NOT NULL,
  `idUsuario` VARCHAR(45) NOT NULL,
  `idTiraje` INT(100) NOT NULL,
  `correlativo` VARCHAR(100) NULL,
  `fecha` DATETIME NULL,
  `condicion_pago` VARCHAR(45) NULL,
  `estatus` VARCHAR(45) NULL,
  `iva_retenido` DOUBLE NULL,
  `subTotal` DOUBLE NULL,
  `ventas_exentas` DOUBLE NULL,
  `ventasNoSujetas` DOUBLE NULL,
  `cantidadEnLetras` VARCHAR(500) NULL,
  `TOTAL` DOUBLE NULL,
  PRIMARY KEY (`idVentas`),
  INDEX `fk_Clientes1` (`idClientes` ASC),
  INDEX `fk_Usuario1` (`idUsuario` ASC),
  INDEX `fk_Tiraje1` (`idTiraje` ASC),
  CONSTRAINT `fk_Clientes1`
    FOREIGN KEY (`idClientes`)
    REFERENCES `sistemaferreteria`.`clientes` (`idClientes`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuario1`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `sistemaferreteria`.`usuarios` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tiraje1`
    FOREIGN KEY (`idTiraje`)
    REFERENCES `sistemaferreteria`.`tiraje` (`idTiraje`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaferreteria`.`detalles_ventas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`detalles_ventas` (
  `idDetalleVenta` INT(11) NULL AUTO_INCREMENT,
  `idVentas` INT(11) NOT NULL,
  `idInventarios` INT(11) NOT NULL,
  `cantidad` DOUBLE NULL DEFAULT NULL,
  `descripcion` VARCHAR(105) NULL DEFAULT NULL,
  `precioUnitario` DOUBLE NULL DEFAULT NULL,
  `ventas_exentas` DOUBLE NULL DEFAULT NULL,
  `ventas_afectadas` DOUBLE NULL DEFAULT NULL,
  `ventasNoSujetas` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`idDetalleVenta`),
  INDEX `fk_Ventas1` (`idVentas` ASC),
  INDEX `fk_Inventario6` (`idInventarios` ASC),
  CONSTRAINT `fk_Inventario6`
    FOREIGN KEY (`idInventarios`)
    REFERENCES `sistemaferreteria`.`inventarios` (`idInventarios`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ventas1`
    FOREIGN KEY (`idVentas`)
    REFERENCES `sistemaferreteria`.`ventas` (`idVentas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaferreteria`.`empresa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`empresa` (
  `idEmpresas` INT(11) NULL AUTO_INCREMENT,
  `nombre` VARCHAR(65) NOT NULL,
  `nit` VARCHAR(15) NULL,
  `nrc` VARCHAR(15) NULL DEFAULT NULL,
  `giro` VARCHAR(45) NULL,
  `telefono` VARCHAR(15) NULL DEFAULT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  `direccion` VARCHAR(45) NULL DEFAULT NULL,
  `municipio` VARCHAR(45) NULL,
  `departamento` VARCHAR(45) NULL,
  `iva_actual` DOUBLE NULL,
  PRIMARY KEY (`idEmpresas`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaferreteria`.`privilegios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`privilegios` (
  `idPrivilegio` INT(11) NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL DEFAULT NULL,
  `accion` VARCHAR(45) NULL,
  PRIMARY KEY (`idPrivilegio`),
  INDEX `fkPadre` (`idPrivilegio` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaferreteria`.`permisos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`permisos` (
  `idPermisos` INT(11) NULL AUTO_INCREMENT,
  `idRol` INT(11) NOT NULL,
  `idPrivilegio` INT(11) NOT NULL,
  PRIMARY KEY (`idPermisos`),
  INDEX `fkRol` (`idRol` ASC),
  INDEX `fkCodigo` (`idPrivilegio` ASC),
  CONSTRAINT `fkRol`
    FOREIGN KEY (`idRol`)
    REFERENCES `sistemaferreteria`.`roles` (`idRoles`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkCodigo`
    FOREIGN KEY (`idPrivilegio`)
    REFERENCES `sistemaferreteria`.`privilegios` (`idPrivilegio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaferreteria`.`log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`log` (
  `idLog` INT(100) NOT NULL AUTO_INCREMENT,
  `idUsuario` VARCHAR(45) NOT NULL,
  `operacion` VARCHAR(100) NULL DEFAULT NULL,
  `descripcion` VARCHAR(100) NULL DEFAULT NULL,
  `fecha` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`idLog`),
  INDEX `fk_Usuarios3` (`idUsuario` ASC),
  CONSTRAINT `fk_Usuarios3`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `sistemaferreteria`.`usuarios` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



-- -----------------------------------------------------
-- Table `sistemaferreteria`.`usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`tiraje` (
  `idTiraje` INT(100) NOT NULL AUTO_INCREMENT,
  `num_ini` int(100) NULL,
  `num_fin` int(100) NULL,
  `correlativo` int(100) NULL,
  `serie` VARCHAR(45) NULL,
  `estado` VARCHAR(45) NULL,
  `tipo` VARCHAR(45) NULL,
  PRIMARY KEY (`idTiraje`)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;




-- -----------------------------------------------------
-- Table `sistemaferreteria`.`compras`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`entradas` (
  `idEntradas` INT(11) NULL AUTO_INCREMENT,
  `idUsuario` VARCHAR(45) NOT NULL,
  `correlativo` VARCHAR(100) NULL,
  `fecha` DATETIME NULL,
  `estatus` VARCHAR(45) NULL,
  `subTotal` DOUBLE NULL,
  `TOTAL` DOUBLE NULL,
  PRIMARY KEY (`idEntradas`),
  INDEX `fk_Usuario2` (`idUsuario` ASC),
	CONSTRAINT `fk_Usuario2`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `sistemaferreteria`.`usuarios` (`idUsuario`)
    ON DELETE NO ACTION
	ON UPDATE NO ACTION
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaferreteria`.`detalles_compras`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`detalles_entradas` (
  `idDetEntradas` INT(11) NULL AUTO_INCREMENT,
  `idEntradas` INT(11) NOT NULL,
  `idInventario` INT(11) NOT NULL,
  `cantidad` DOUBLE NULL DEFAULT NULL,
  `descripcion` VARCHAR(105) NULL DEFAULT NULL,
  `precioUnitario` DOUBLE NULL DEFAULT NULL,
  `total` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`idDetEntradas`),
  INDEX `fk_Entradas1` (`idEntradas` ASC),
  INDEX `fk_Inventarios3` (`idInventario` ASC),
  CONSTRAINT `fk_Entradas1`
    FOREIGN KEY (`idEntradas`)
    REFERENCES `sistemaferreteria`.`entradas` (`idEntradas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Inventarios3`
    FOREIGN KEY (`idInventario`)
    REFERENCES `sistemaferreteria`.`inventarios` (`idInventarios`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaferreteria`.`ventas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`salidas` (
  `idSalidas` INT(11) NULL AUTO_INCREMENT,
  `idUsuario` VARCHAR(45) NOT NULL,
  `correlativo` VARCHAR(100) NULL,
  `fecha` DATETIME NULL,
  `estatus` VARCHAR(45) NULL,
  `subTotal` DOUBLE NULL,
  `TOTAL` DOUBLE NULL,
  PRIMARY KEY (`idSalidas`),
  INDEX `fk_Usuario3` (`idUsuario` ASC),
	CONSTRAINT `fk_Usuario3`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `sistemaferreteria`.`usuarios` (`idUsuario`)
    ON DELETE NO ACTION
	ON UPDATE NO ACTION
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaferreteria`.`detalles_ventas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistemaferreteria`.`detalles_salidas` (
  `idDetSalidas` INT(11) NULL AUTO_INCREMENT,
  `idSalidas` INT(11) NOT NULL,
  `idInventarios` INT(11) NOT NULL,
  `cantidad` DOUBLE NULL DEFAULT NULL,
  `descripcion` VARCHAR(105) NULL DEFAULT NULL,
  `precioUnitario` DOUBLE NULL DEFAULT NULL,
  `total` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`idDetSalidas`),
  INDEX `fk_Salidas1` (`idSalidas` ASC),
  INDEX `fk_Inventario7` (`idInventarios` ASC),
  CONSTRAINT `fk_Inventario7`
    FOREIGN KEY (`idInventarios`)
    REFERENCES `sistemaferreteria`.`inventarios` (`idInventarios`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Salidas1`
    FOREIGN KEY (`idSalidas`)
    REFERENCES `sistemaferreteria`.`salidas` (`idSalidas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- !!-----------------------------------------------------
-- Triggers
-- !!-----------------------------------------------------
-- para aumentar las existencias

delimiter |
-- despues de haber insertado en detalle compras que actualize la cantidad en inventarioss
CREATE TRIGGER cantidad_prod_compra after INSERT ON detalles_compras
  FOR EACH ROW
  BEGIN
    UPDATE inventarios SET inventarios.existencias = inventarios.existencias + NEW.cantidad  WHERE NEW.idInventario = inventarios.idInventarios;
    
    UPDATE subcategorias s1, (select i.idSubCategorias as idSub from inventarios i where i.idInventarios=NEW.idInventario) as s2
    SET s1.TotalProducto = s1.TotalProducto + NEW.cantidad
    WHERE s1.idSubCategorias = s2.idSub;
    
  END;
|

delimiter ;


-- drop trigger cantidad_prod_compra;


-- para disminuir las existencias
delimiter |
CREATE TRIGGER cantidad_prod_venta after INSERT ON detalles_ventas
  FOR EACH ROW
  BEGIN

    UPDATE inventarios SET inventarios.existencias = inventarios.existencias - NEW.cantidad  WHERE NEW.idInventarios = inventarios.idInventarios;
    
	UPDATE subcategorias s1, (select i.idSubCategorias as idSub from inventarios i where i.idInventarios=NEW.idInventarios) as s2
    SET s1.TotalProducto = s1.TotalProducto - NEW.cantidad
    WHERE s1.idSubCategorias = s2.idSub;
    
  END;
|

delimiter ;

-- drop trigger cantidad_prod_venta;

delimiter  |

-- DESPUES DE HABER INSERTADO EN DETALLE ENTRADAS QUE ACTUALICE LA CANTIDAD EN INVENTARIO

CREATE TRIGGER cantidad_det_compra after INSERT ON detalles_entradas
FOR EACH ROW
BEGIN
	UPDATE inventarios SET inventarios.existencias = inventarios.existencias + NEW.cantidad WHERE NEW.idInventario = inventarios.idInventarios;
    
	UPDATE subcategorias s1, (select i.idSubCategorias as idSub from inventarios i where i.idInventarios=NEW.idInventario) as s2
    SET s1.TotalProducto = s1.TotalProducto + NEW.cantidad
    WHERE s1.idSubCategorias = s2.idSub;
END;

|

delimiter ;

-- PARA DISMINUIR EXISTENCIAS

delimiter  |

CREATE TRIGGER cantidad_prod_salidas after INSERT ON detalles_salidas
FOR EACH ROW 
BEGIN 
	UPDATE inventarios SET inventarios.existencias = inventarios.existencias - NEW.cantidad WHERE NEW.idInventarios = inventarios.idInventarios;
    
    UPDATE subcategorias s1, (select i.idSubCategorias as idSub from inventarios i where i.idInventarios=NEW.idInventarios) as s2
    SET s1.TotalProducto = s1.TotalProducto - NEW.cantidad
    WHERE s1.idSubCategorias = s2.idSub;
END;

|

delimiter ;



-- restaura la cantidad cuando se elimina detalle venta

delimiter |
CREATE TRIGGER detalle_venta_eliminado before delete ON detalles_ventas
  FOR EACH ROW
  BEGIN

    UPDATE inventarios SET inventarios.existencias = inventarios.existencias + OLD.cantidad  WHERE OLD.idInventarios = inventarios.idInventarios;
    
	UPDATE subcategorias s1, (select i.idSubCategorias as idSub from inventarios i where i.idInventarios=OLD.idInventarios) as s2
    SET s1.TotalProducto = s1.TotalProducto + OLD.cantidad
    WHERE s1.idSubCategorias = s2.idSub;
  END;
|

delimiter ;

-- elimina la cantidad cuando se elimina detalle compra

delimiter |

-- actualizando detalle compra 642
CREATE TRIGGER detalle_compra_eliminado before delete ON detalles_compras
  FOR EACH ROW
  BEGIN

    UPDATE inventarios SET inventarios.existencias = inventarios.existencias - OLD.cantidad  WHERE OLD.idInventario = inventarios.idInventarios;
    
	UPDATE subcategorias s1, (select i.idSubCategorias as idSub from inventarios i where i.idInventarios=OLD.idInventario) as s2
    SET s1.TotalProducto = s1.TotalProducto - OLD.cantidad
    WHERE s1.idSubCategorias = s2.idSub;
  END;
|

delimiter ;


-- restaura la cantidad cuando detalle salida eliminado 

delimiter |
CREATE TRIGGER detalle_salida_eliminado before delete ON detalles_salidas
  FOR EACH ROW
  BEGIN

    UPDATE inventarios SET inventarios.existencias = inventarios.existencias + OLD.cantidad  WHERE OLD.idInventarios = inventarios.idInventarios;
    
	UPDATE subcategorias s1, (select i.idSubCategorias as idSub from inventarios i where i.idInventarios=OLD.idInventarios) as s2
    SET s1.TotalProducto = s1.TotalProducto + OLD.cantidad
    WHERE s1.idSubCategorias = s2.idSub;
    
  END;
|

delimiter ;

-- eliminar el producto cuando detalle entrada eliminado

delimiter |
CREATE TRIGGER detalle_entrada_eliminado before delete ON detalles_entradas
  FOR EACH ROW
  BEGIN

    UPDATE inventarios SET inventarios.existencias = inventarios.existencias - OLD.cantidad  WHERE OLD.idInventario = inventarios.idInventarios;
    
	UPDATE subcategorias s1, (select i.idSubCategorias as idSub from inventarios i where i.idInventarios=OLD.idInventario) as s2
    SET s1.TotalProducto = s1.TotalProducto - OLD.cantidad
    WHERE s1.idSubCategorias = s2.idSub;
    
  END;
|

delimiter ;



 delimiter |
 CREATE TRIGGER actualizar_categoria after update ON subcategorias
  FOR EACH ROW
  BEGIN
    UPDATE categorias i1, (select sum(s.TotalProducto) as total,c.idCategorias as id from subcategorias s, categorias c where s.idCategoria = c.idCategorias  group by s.idCategoria) i2
    SET i1.TotalProducto = i2.total
    WHERE i1.idCategorias = i2.id and i1.idCategorias = NEW.idCategoria;
  END;
 |

 delimiter ;

-- !!-----------------------------------------------------
-- Inserciones
-- !!-----------------------------------------------------

-- categorias

INSERT INTO categorias (NombreCategoria,TotalProducto)
      VALUES ('herramientas',0);
INSERT INTO categorias (NombreCategoria,TotalProducto)
      VALUES ('MaterialesConstruccion',0);
INSERT INTO categorias (NombreCategoria,TotalProducto)
      VALUES ('Clavos',0);
      
-- Subcategorias

INSERT INTO subcategorias (NombreSubCategoria,TotalProducto,idCategoria)
      VALUES ('llaves',0,1);
INSERT INTO subcategorias (NombreSubCategoria,TotalProducto,idCategoria)
      VALUES ('Cementos',0,2);
INSERT INTO subcategorias (NombreSubCategoria,TotalProducto,idCategoria)
      VALUES ('Acerados',0,3);
      
      
-- roles


INSERT INTO roles (rol)
      VALUES ('Administrador');
INSERT INTO roles (rol)
      VALUES ('Empleado1');
INSERT INTO roles (rol)
      VALUES ('Empleado2');

-- usuarios

INSERT INTO usuarios (idUsuario,idRoles,nombres,apellidos,
clave,direccion,telefono,email,fecha_ingreso,dui,nit,sueldo)
      VALUES ('user',1,'Herni botbol','Alvarado Robles',sha1(sha1('12e53f47ec')),'Sonsonate col 14','71254878',
      'herni_robles@gmail.com',sysdate(),'0000-000-0000-1','7-2014-254682',450);
INSERT INTO usuarios (idUsuario,idRoles,nombres,apellidos,
clave,direccion,telefono,email,fecha_ingreso,dui,nit,sueldo)
      VALUES ('user2',2,'claudia padilla','linares Robles',sha1(sha1('claudia_padmag')),'Sonsonate col 14','71527456',
      'smalvarado@gmail.com',sysdate(),'0000-000-2000-1','7-2714-254672',450);
      
INSERT INTO usuarios (idUsuario,idRoles,nombres,apellidos,
clave,direccion,telefono,email,fecha_ingreso,dui,nit,sueldo)
      VALUES ('user3',2,'Jonathan Geovany','Hernandez Vasquez',sha1(sha1('jonathan_geovany')),'Sonsonate col 14','66854878',
      'jc_pr@gmail.com',sysdate(),'7040-000-0000-1','8-2814-254682',450);
      
-- clientes

INSERT INTO clientes (idUsuario,nombres,apellidos,direccion,telefono,dui,nit,nrc)
      VALUES ('user','Pedro Pablo','Sanchez Vazquez','Sonsonate','61287878','2500-700-0250-1','1-2015-287982','45020-1588');

INSERT INTO clientes (idUsuario,nombres,apellidos,direccion,telefono,dui,nit,nrc)
      VALUES ('user2','Juan paolo','luers picaso','Sonsonate','61287878','2500-700-0250-1','1-2015-287982','45020-1588');

INSERT INTO clientes (idUsuario,nombres,apellidos,direccion,telefono,dui,nit,nrc)
      VALUES ('user3','Claudia Patricia','linares sanches','Sonsonate','61287878','2500-700-0250-1','1-2015-287982','45020-1588');
      
-- proveedores

INSERT INTO proveedores (nombres,apellidos,
direccion,DUI,NIT,telefono,email,NRC)
      VALUES ('Maria Julia','Guillen Moz','Sonsonate ','2454-1414',
      '9600-700-0250-1','78549658','MariaJulia@gmail.com','7854899');
      
INSERT INTO proveedores (nombres,apellidos,
direccion,DUI,NIT,telefono,email,NRC)
      VALUES ('Mauricio Jose','Solorzano Peña','Sonsonate ','2485-1784',
      '1-0000-700-0250-1','75489625','MauricioJose@gmail.com','1587-96');
      
INSERT INTO proveedores (nombres,apellidos,
direccion,DUI,NIT,telefono,email,NRC)
      VALUES ('Walter Omar','Mendez Cigaran','Sonsonate ','2254-1204',
      '1-8500-100-0250-1','78415968','WalterOmar@gmail.com','7854-987');
      
-- agregando compras

INSERT INTO compras (
  idUsuario ,
  idProveedores ,
  correlativo,
  fecha,
  condicion_pago,
  estatus,
  iva,
  iva_percibido,
  iva_retenido,
  subTotal,
  -- ventas_exentas,
  -- ventasNoSujetas,
  TOTAL)
      VALUES ('user',2,12,sysdate(),
      'contado','pagado',0,0,0,450,450);


INSERT INTO compras (
  idUsuario ,
  idProveedores ,
  correlativo,
  fecha,
  condicion_pago,
  estatus,
  iva,
  iva_percibido,
  iva_retenido,
  subTotal,
  -- ventas_exentas,
  -- ventasNoSujetas,
  TOTAL)
      VALUES ('user',1,14,sysdate(),
      'contado','pagado',0,0,0,2600,2600);

INSERT INTO compras (
  idUsuario ,
  idProveedores ,
  correlativo,
  fecha,
  condicion_pago,
  estatus,
  iva,
  iva_percibido,
  iva_retenido,
  subTotal,
  -- ventas_exentas,
  -- ventasNoSujetas,
  TOTAL)
      VALUES ('user',1,15,sysdate(),
      'contado','pagado',0,0,0,1150,1150);
      
      INSERT INTO compras (
  idUsuario ,
  idProveedores ,
  correlativo,
  fecha,
  condicion_pago,
  estatus,
  iva,
  iva_percibido,
  iva_retenido,
  subTotal,
  -- ventas_exentas,
  -- ventasNoSujetas,
  TOTAL)
      VALUES ('user',1,16,sysdate(),
      'contado','pagado',0,0,0,875,875);
      
      INSERT INTO compras (
  idUsuario ,
  idProveedores ,
  correlativo,
  fecha,
  condicion_pago,
  estatus,
  iva,
  iva_percibido,
  iva_retenido,
  subTotal,
  -- ventas_exentas,
  -- ventasNoSujetas,
  TOTAL)
      VALUES ('user',1,17,sysdate(),
      'contado','pagado',0,0,0,520,520);
      
-- agregando marcas

INSERT INTO marcas_productos (
  marca )
      VALUES ('Holcim');
INSERT INTO marcas_productos (
  marca )
      VALUES ('hogar');
INSERT INTO marcas_productos (
  marca )
      VALUES ('cuscatlan');
      
-- ubicaciones 

INSERT INTO ubicaciones (
  ubicacion )
      VALUES ('Pasio1');
INSERT INTO ubicaciones (
  ubicacion )
      VALUES ('Pasio4');
INSERT INTO ubicaciones (
  ubicacion )
      VALUES ('Pasio8');
      
      
-- agregando inventarios


INSERT INTO inventarios  ( 
  idUbicaciones,
  idSubCategorias,
  idMarcas_Productos,
  descripcion,
  stock_maximo,
  stock_minimo,
  existencias,
  precio_compra,
  precio_venta,
  descuento
 )VALUES (1,1,1,'varia de Hierro 1/4',300,30,0,1.50,3.50,0.0);
 
INSERT INTO inventarios  ( 
  idUbicaciones,
  idSubCategorias,
  idMarcas_Productos,
  descripcion,
  stock_maximo,
  stock_minimo,
  existencias,
  precio_compra,
  precio_venta,
  descuento
 )VALUES (2,2,2,'martillo',30,5,0,5,10,0.0);
 

INSERT INTO inventarios  ( 
  idUbicaciones,
  idSubCategorias,
  idMarcas_Productos,
  descripcion,
  stock_maximo,
  stock_minimo,
  existencias,
  precio_compra,
  precio_venta,
  descuento
 )VALUES (1,3,1,'bolsa de cemento',1000,30,0,4,7.50,0.0);
 
INSERT INTO inventarios  ( 
  idUbicaciones,
  idSubCategorias,
  idMarcas_Productos,
  descripcion,
  stock_maximo,
  stock_minimo,
  existencias,
  precio_compra,
  precio_venta,
  descuento
 )VALUES (1,3,1,'bolsa de pegamix',250,30,0,3,6,0.0);


INSERT INTO inventarios  ( 
  idUbicaciones,
  idSubCategorias,
  idMarcas_Productos,
  descripcion,
  stock_maximo,
  stock_minimo,
  existencias,
  precio_compra,
  precio_venta,
  descuento
 )VALUES (1,3,1,'taladro electrico',30,3,0,20,35,0.0);
 
 
INSERT INTO inventarios  ( 
  idUbicaciones,
  idSubCategorias,
  idMarcas_Productos,
  descripcion,
  stock_maximo,
  stock_minimo,
  existencias,
  precio_compra,
  precio_venta,
  descuento
 )VALUES (1,3,1,'ladrillo rojo',8000,300,0,0.15,0.35,0.0);

INSERT INTO inventarios  ( 
  idUbicaciones,
  idSubCategorias,
  idMarcas_Productos,
  descripcion,
  stock_maximo,
  stock_minimo,
  existencias,
  precio_compra,
  precio_venta,
  descuento
 )VALUES (1,3,1,'Bloque de cemento',3000,300,0,0.25,0.50,0.0);
 
 
INSERT INTO inventarios  ( 
  idUbicaciones,
  idSubCategorias,
  idMarcas_Productos,
  descripcion,
  stock_maximo,
  stock_minimo,
  existencias,
  precio_compra,
  precio_venta,
  descuento
 )VALUES (1,1,1,'metro de alambre',800,30,0,0.50,2.50,0.0);
 
 
 
INSERT INTO inventarios  ( 
  idUbicaciones,
  idSubCategorias,
  idMarcas_Productos,
  descripcion,
  stock_maximo,
  stock_minimo,
  existencias,
  precio_compra,
  precio_venta,
  descuento
 )VALUES (1,1,1,'pala de metal',100,5,0,6.50,10,0.0);
 
 
-- agregando ventas detalles

-- select * from detalles_compras;

 
INSERT INTO detalles_compras  ( idCompras, idInventario,cantidad,descripcion,precioUnitario,
  ventas_exentas,ventasNoSujetas,ventas_gravadas
 )VALUES (1,2,30,'martillo',5,0,0,150);
 
      
INSERT INTO detalles_compras  ( idCompras,idInventario,cantidad,descripcion,precioUnitario,
  ventas_exentas,ventasNoSujetas,ventas_gravadas
 )VALUES (1,1,200,'varilla 1/4',1.5,0,0,300);
 
 INSERT INTO detalles_compras  ( idCompras,idInventario,cantidad,descripcion,precioUnitario,
  ventas_exentas,ventasNoSujetas,ventas_gravadas
 )VALUES (2,3,500,'bolsa de cemento',4,0,0,2000);
 
 INSERT INTO detalles_compras  ( idCompras,idInventario,cantidad,descripcion,precioUnitario,
  ventas_exentas,ventasNoSujetas,ventas_gravadas
 )VALUES (2,4,200,'bolsa de pegaminx',3,0,0,600);
 
 INSERT INTO detalles_compras  ( idCompras,idInventario,cantidad,descripcion,precioUnitario,
  ventas_exentas,ventasNoSujetas,ventas_gravadas
 )VALUES (3,5,20,'taladro electrico',20,0,0,400);
 
 INSERT INTO detalles_compras  ( idCompras,idInventario,cantidad,descripcion,precioUnitario,
  ventas_exentas,ventasNoSujetas,ventas_gravadas
 )VALUES (3,6,5000,'ladrillo rojo',0.15,0,0,750);
 
 INSERT INTO detalles_compras  ( idCompras,idInventario,cantidad,descripcion,precioUnitario,
  ventas_exentas,ventasNoSujetas,ventas_gravadas
 )VALUES (4,7,2500,'bloque de cemento',0.25,0,0,625);
 
 INSERT INTO detalles_compras  ( idCompras,idInventario,cantidad,descripcion,precioUnitario,
  ventas_exentas,ventasNoSujetas,ventas_gravadas
 )VALUES (4,8,500,'metro de alambre',0.50,0,0,250);
 
 INSERT INTO detalles_compras  ( idCompras,idInventario,cantidad,descripcion,precioUnitario,
  ventas_exentas,ventasNoSujetas,ventas_gravadas
 )VALUES (5,9,80,'pala de metal',6.5,0,0,520);
 -- 
 
 -- ventas
 
-- select * from subcategorias;
 
-- select * from inventarios;


insert into tiraje (num_ini, num_fin, correlativo, serie, estado, tipo)
values (0, 200, 4, 'A001S', 'activo', 'factura contado');

insert into tiraje (num_ini, num_fin, correlativo, serie, estado, tipo)
values (0, 200, 1, 'A001E', 'desactivado', 'factura contado');

-- select * from tiraje;


INSERT INTO ventas  (idClientes,idUsuario,idTiraje,correlativo,fecha,condicion_pago,estatus,iva_retenido,
subTotal,ventas_exentas,ventasNoSujetas,cantidadEnLetras,TOTAL)
VALUES (1,'user',1,1,sysdate(),'contado ','retenida',0,135.0,0,0,'ciento treinta y cinco con 0.0',135.0);


INSERT INTO ventas  (idClientes,idUsuario,idTiraje,correlativo,fecha,condicion_pago,estatus,iva_retenido,
subTotal,ventas_exentas,ventasNoSujetas,cantidadEnLetras,TOTAL)
VALUES (2,'user',1,1,sysdate(),'contado ','retenida',0,135.0,0,0,'ciento treinta y cinco con 0.0',135.0);


INSERT INTO ventas  (idClientes,idUsuario,idTiraje,correlativo,fecha,condicion_pago,estatus,iva_retenido,
subTotal,ventas_exentas,ventasNoSujetas,cantidadEnLetras,TOTAL)
VALUES (3,'user',1,1,sysdate(),'contado ','retenida',0,70,0,0,'setenta con 0.0',70);
                  

-- detalle ventas
      
INSERT INTO detalles_ventas  ( idVentas,idInventarios,cantidad,descripcion,precioUnitario,
  ventas_exentas,ventas_afectadas,ventasNoSujetas
 )
      VALUES (1,1,10,'varia de hierro 1/4',3.5,0,35,0);

      
INSERT INTO detalles_ventas  ( idVentas,idInventarios,cantidad,descripcion,precioUnitario,
  ventas_exentas,ventas_afectadas,ventasNoSujetas
 )
      VALUES (1,2,10,'cementos',10,0,100,0);
      
INSERT INTO detalles_ventas  ( idVentas,idInventarios,cantidad,descripcion,precioUnitario,
  ventas_exentas,ventas_afectadas,ventasNoSujetas
 )
      VALUES (2,3,10,'cementos',7.5,0,75,0);
      
INSERT INTO detalles_ventas  ( idVentas,idInventarios,cantidad,descripcion,precioUnitario,
  ventas_exentas,ventas_afectadas,ventasNoSujetas
 )
      VALUES (2,4,10,'cementos',6,0,60,0);   
      
INSERT INTO detalles_ventas  ( idVentas,idInventarios,cantidad,descripcion,precioUnitario,
  ventas_exentas,ventas_afectadas,ventasNoSujetas
 )
      VALUES (3,4,2,'cementos',35,0,70,0);   
      
-- conversion

      

-- privilegios

INSERT INTO privilegios(  nombre,accion) VALUES ('ventas','0');
INSERT INTO privilegios (  nombre,accion) VALUES ('nueva venta','1');
INSERT INTO privilegios (  nombre,accion) VALUES ('consultar venta','2');
INSERT INTO privilegios (  nombre,accion) VALUES ('Clientes','3');
INSERT INTO privilegios(  nombre,accion) VALUES ('tiraje','4');
INSERT INTO privilegios (  nombre,accion) VALUES ('nueva salida','5');
INSERT INTO privilegios (  nombre,accion) VALUES ('consultar salidas','6');

INSERT INTO privilegios (  nombre,accion) VALUES ('compras','7');
INSERT INTO privilegios(  nombre,accion) VALUES ('nueva compra','8');
INSERT INTO privilegios (  nombre,accion) VALUES ('consultar compra','9');
INSERT INTO privilegios (  nombre,accion) VALUES ('Proveedores','10');
INSERT INTO privilegios (  nombre,accion) VALUES ('nueva entrada','11');
INSERT INTO privilegios(  nombre,accion) VALUES ('consultar entradas','12');

INSERT INTO privilegios (  nombre,accion) VALUES ('inventarios','13');
INSERT INTO privilegios (  nombre,accion) VALUES ('productos','14');
INSERT INTO privilegios (  nombre,accion) VALUES ('categorias','15');
INSERT INTO privilegios(  nombre,accion) VALUES ('marcas','16');
INSERT INTO privilegios (  nombre,accion) VALUES ('ubicaciones','17');

INSERT INTO privilegios (  nombre,accion) VALUES ('reportes','18');
INSERT INTO privilegios (  nombre,accion) VALUES ('generar reporte','19');

INSERT INTO privilegios(  nombre,accion) VALUES ('configuraciones','20');
INSERT INTO privilegios (  nombre,accion) VALUES ('datos generales','21');
INSERT INTO privilegios (  nombre,accion) VALUES ('usuarios','22');
INSERT INTO privilegios (  nombre,accion) VALUES ('roles - privilegios','23');
INSERT INTO privilegios(  nombre,accion) VALUES ('log','24');


-- privilegios a permisos

-- para admin

insert into permisos (idRol,idPrivilegio) values (1,1);
insert into permisos (idRol,idPrivilegio) values (1,2);
insert into permisos (idRol,idPrivilegio) values (1,3);
insert into permisos (idRol,idPrivilegio) values (1,4);
insert into permisos (idRol,idPrivilegio) values (1,5);
insert into permisos (idRol,idPrivilegio) values (1,6);
insert into permisos (idRol,idPrivilegio) values (1,7);
insert into permisos (idRol,idPrivilegio) values (1,8);
insert into permisos (idRol,idPrivilegio) values (1,9);
insert into permisos (idRol,idPrivilegio) values (1,10);
insert into permisos (idRol,idPrivilegio) values (1,11);
insert into permisos (idRol,idPrivilegio) values (1,12);
insert into permisos (idRol,idPrivilegio) values (1,13);
insert into permisos (idRol,idPrivilegio) values (1,14);
insert into permisos (idRol,idPrivilegio) values (1,15);
insert into permisos (idRol,idPrivilegio) values (1,16);
insert into permisos (idRol,idPrivilegio) values (1,17);
insert into permisos (idRol,idPrivilegio) values (1,18);
insert into permisos (idRol,idPrivilegio) values (1,19);
insert into permisos (idRol,idPrivilegio) values (1,20);
insert into permisos (idRol,idPrivilegio) values (1,21);
insert into permisos (idRol,idPrivilegio) values (1,22);
insert into permisos (idRol,idPrivilegio) values (1,23);
insert into permisos (idRol,idPrivilegio) values (1,24);
insert into permisos (idRol,idPrivilegio) values (1,25);


-- para user

insert into permisos (idRol,idPrivilegio) values (2,1);
insert into permisos (idRol,idPrivilegio) values (2,2);
insert into permisos (idRol,idPrivilegio) values (2,3);
insert into permisos (idRol,idPrivilegio) values (2,4);
insert into permisos (idRol,idPrivilegio) values (2,5);
insert into permisos (idRol,idPrivilegio) values (2,6);
insert into permisos (idRol,idPrivilegio) values (2,7);
insert into permisos (idRol,idPrivilegio) values (2,8);
insert into permisos (idRol,idPrivilegio) values (2,9);
insert into permisos (idRol,idPrivilegio) values (2,10);
insert into permisos (idRol,idPrivilegio) values (2,11);
insert into permisos (idRol,idPrivilegio) values (2,12);
insert into permisos (idRol,idPrivilegio) values (2,13);
insert into permisos (idRol,idPrivilegio) values (2,14);
insert into permisos (idRol,idPrivilegio) values (2,15);
insert into permisos (idRol,idPrivilegio) values (2,16);
insert into permisos (idRol,idPrivilegio) values (2,17);
insert into permisos (idRol,idPrivilegio) values (2,18);
insert into permisos (idRol,idPrivilegio) values (2,19);
insert into permisos (idRol,idPrivilegio) values (2,20);
insert into permisos (idRol,idPrivilegio) values (2,21);
insert into permisos (idRol,idPrivilegio) values (2,22);
insert into permisos (idRol,idPrivilegio) values (2,23);
insert into permisos (idRol,idPrivilegio) values (2,24);
insert into permisos (idRol,idPrivilegio) values (2,25);


-- insercion tabla empresa 
insert into empresa (nombre, nit, nrc, giro, telefono, email, direccion, municipio, departamento, iva_actual)
	values ('JC & Asociados', '1548-154686-124', '7896523', 'comercial', '7894-2456', 'JC&Asociados@gmail.com', 'col. 14 sonsonate', 'sonsonate', 'sonsonate', '0.13');



-- CREACION DE USUARIO
CREATE USER 'pmvh16'@'%' IDENTIFIED BY 'cecdaa836d7e3986d81c71568e43f8a4e1ba440f';

GRANT SELECT,UPDATE,INSERT,DELETE ON sistemaferreteria.* To pmvh16@'%' identified by 'cecdaa836d7e3986d81c71568e43f8a4e1ba440f';

FLUSH PRIVILEGES;

-- REVOKE all ON *.* FROM 'pmvh16'@'%';

-- REVOKE Create User ON *.* FROM 'pmvh16'@'localhost';

-- permite crear nuevas tablas o base de datos
-- GRANT CREATE  ON *.* TO 'pmvh16'@'localhost';

-- permite eliminar registros en  tablas 
-- GRANT DELETE  ON *.* TO 'pmvh16'@'localhost';

-- permite insertar registros en  tablas 
-- GRANT INSERT  ON *.* TO 'pmvh16'@'localhost';

-- permite leer registros en  tablas 
-- GRANT SELECT  ON *.* TO 'pmvh16'@'localhost';

-- permite actualizar registros en  tablas 
-- GRANT UPDATE  ON *.* TO 'pmvh16'@'localhost';






-- DAR TODOS LOS PRIVILEGIOS los asterios significan la base de datos y la tabla a la cual el nuevo usuario tendra acceso
-- GRANT ALL PRIVILEGES ON * . * TO 'nombre_usuario'@'localhost';





-- PARA REMOVER PERMISOS
-- REVOKE [permiso] ON [nombre de base de datos].[nombre de tabla] FROM ‘[nombre de usuario]’@‘localhost’;

-- BORRAR USUARIOS
-- DROP USER ‘usuario_prueba’@‘localhost’;
