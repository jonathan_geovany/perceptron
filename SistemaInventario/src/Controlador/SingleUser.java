/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

/**
 *
 * @author Jonathan
 */
public class SingleUser {
    
    private String nombres="";
    private String rol="";
    private String idUsuario="";
    private Integer idRol;
    private static SingleUser instancia= null;
    
    protected SingleUser(){
        //conectado = conectar();
    }
    
    public static SingleUser getInstancia() {
      if(instancia == null) {
         instancia = new SingleUser();
      }
      return instancia;
   }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the idRol
     */
    public Integer getIdRol() {
        return idRol;
    }

    /**
     * @param idRol the idRol to set
     */
    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }
    
}
