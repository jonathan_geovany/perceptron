/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//11/06/2016
package Controlador;

import java.awt.Component;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.hibernate.transform.Transformers;
/**
 *
 * @author Jonathan
 */
public class Operaciones {
    
    private static Operaciones instancia= null;
    private SessionFactory sesion;
    
    private String host="45.55.43.106";
    private String bd="sistemaferreteria";
    private String puerto="3306";
    private String user="pmvh16";
    private String pass="cecdaa836d7e3986d81c71568e43f8a4e1ba440f";
    private String cadena;
    
    private boolean conectado=false;
    
    protected Operaciones(){
        conectado = conectar();
    }
    
    public Connection getConexion(){
        Connection conexion=null;
        try{
            conexion = DriverManager.getConnection(cadena,user,pass);
        }catch(Exception exCon){
            return null;
        }
        return conexion;
    }
    
    
    public boolean conectar(){
        if(conectado){
            this.cerrarConexion();
        }
        Configuration config = new Configuration();
        config.configure();
        cadena = "jdbc:mysql://"+host+":"+puerto+"/"+ bd +"?zeroDateTimeBehavior=convertToNull";
        try{
            config.setProperty("hibernate.connection.username", user);
            config.setProperty("hibernate.connection.password", pass);
            config.setProperty("hibernate.connection.url",cadena);
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();        
            sesion = config.buildSessionFactory(serviceRegistry);
            conectado = true;
        }catch(Exception Ex){
            //JOptionPane.showMessageDialog(null, "Error : "+Ex.getMessage());
            conectado = false;
        }
        return conectado;
    } 
    
    public static Operaciones getInstancia() {
      if(instancia == null) {
         instancia = new Operaciones();
      }
      return instancia;
   }
    public boolean guardar(Object objeto){
        return guardar(objeto,true);
    }
   
    
    public boolean guardar(Object objeto,boolean mensaje){
        boolean estado=false;
        if(conectado){
            Session session = sesion.openSession();
            //modelo de transacciones
            Transaction tx = session.beginTransaction();
            try{
                session.saveOrUpdate(objeto);
                tx.commit();
                if(mensaje){
                    JOptionPane.showMessageDialog(null, "Guardado correctamente");
                }
                
                estado=true;
            }catch (HibernateException e) {
                if (tx!=null) tx.rollback();
                e.printStackTrace();
                if(mensaje){
                    JOptionPane.showMessageDialog(null, "Error : " + e.getMessage());
                }
                
            }finally {
                session.close();
                
            }
        }
        return estado;
    }
    
    public boolean actualizar(Object clase){
        if(conectado){    
            Session session = sesion.openSession();
            Transaction tx = session.beginTransaction();
            //modelo de transacciones
            try{
                session.saveOrUpdate(clase);
                tx.commit();
                return true;
            }catch (HibernateException e) {
                if (tx!=null) tx.rollback();
                e.printStackTrace(); 
                JOptionPane.showMessageDialog(null, "Error : " + e.getMessage());
            }finally {
                session.close(); 
            }
        }
        return false;
    }
    
    public Object buscar(Serializable pk,Object clase){
        Object resultado=null;
        if(conectado){    
            Session session = sesion.openSession();
            Transaction tx = session.beginTransaction();
            //modelo de transacciones
            try{
                resultado = session.get(clase.getClass(),pk);
            }catch (HibernateException e) {
                e.printStackTrace(); 
                JOptionPane.showMessageDialog(null, "Error : " + e.getMessage());
            }finally {
                session.close(); 
            }
        }
        return resultado;
    }
    
    
    public void eliminar(Serializable pk,Object clase){
        eliminar(pk,clase,true);
    }
    
    public void eliminar(Serializable pk,Object clase,boolean mensaje){
        
        if(conectado){    
            Session session = sesion.openSession();
            Transaction tx = session.beginTransaction();
            //modelo de transacciones
            
            
            try{
                if(mensaje){
                    int reply = JOptionPane.showConfirmDialog(null, "¿Desea eliminar el dato?", "Advertencia", JOptionPane.YES_NO_OPTION);
                    if (reply == JOptionPane.YES_OPTION) {
                      session.delete(session.get(clase.getClass(),pk));
                      session.getTransaction().commit();
                    }
                }else{
                    session.delete(session.get(clase.getClass(),pk));
                    session.getTransaction().commit();
                }
            }catch (HibernateException e) {
                e.printStackTrace(); 
                session.getTransaction().rollback();
                JOptionPane.showMessageDialog(null, "Error : " + e.getMessage());
            }finally {
                session.close(); 
            }
        }
    }
    
    public List<Object[]> consulta(String hql){
        List<Object[]> datos=null;
        if(conectado){
            
            Session session = sesion.openSession();
            Transaction tx = session.beginTransaction();
            //modelo de transacciones
            try{
                Query q = session.createSQLQuery(hql);
                datos = q.list();
                
            }catch (HibernateException e) {
                if (tx!=null) tx.rollback();
                e.printStackTrace(); 
                JOptionPane.showMessageDialog(null, "Error : " + e.getMessage());
            }finally {
                session.close(); 
            }
        }
        return datos;
    }
    
    
    
    public List<Object> lista(Object clase){
        List<Object> datos=null;
        if(conectado){
            Session session = sesion.openSession();
            Transaction tx = session.beginTransaction();
            //modelo de transacciones
            try{
                String nombre = clase.getClass().getName().substring(7);
                Query q = session.createQuery("from "+ nombre);
                datos = q.list();
            }catch (HibernateException e) {
                if (tx!=null) tx.rollback();
                e.printStackTrace(); 
                JOptionPane.showMessageDialog(null, "Error : " + e.getMessage());
            }finally {
                session.close(); 
            }
        }
        return datos;
    }
    
    public Object buscarPorParametros(String parametros,Object clase){
        Object resultado=null;
        if(conectado){    
            Session session = sesion.openSession();
            Transaction tx = session.beginTransaction();
            //modelo de transacciones
            try{
                List<Object [] > datos = consulta(parametros);
                if(datos!=null){
                    Serializable pk = (Serializable)datos.get(0)[0];
                    resultado = session.get(clase.getClass(),pk);
                }
                
                
            }catch (HibernateException e) {
                e.printStackTrace(); 
                JOptionPane.showMessageDialog(null, "Error : " + e.getMessage());
            }finally {
                session.close(); 
            }
        }
        return resultado;
    }
    
    public void cerrarConexion(){
        if(conectado){
            sesion.close();
        }
    }
    //-----------configuraciones de conexion-----------
    public String getHost() {
        return host;
    }
    public void setHost(String host) {
        this.host = host;
    }
    public String getBd() {
        return bd;
    }
    public void setBd(String bd) {
        this.bd = bd;
    }
    public String getPuerto() {
        return puerto;
    }
    public void setPuerto(String puerto) {
        this.puerto = puerto;
    }
    public String getCadena() {
        return cadena;
    }
    public void setCadena(String cadena) {
        this.cadena = cadena;
    }
    public boolean getConectado() {
        return conectado;
    }
    public String getUser() {
        return user;
    }
    public void setUser(String user) {
        this.user = user;
    }
    public String getPass() {
        return pass;
    }
    public void setPass(String pass) {
        this.pass = pass;
    }
    
    public void setHabilitado(JPanel panel,boolean estado){
     
      panel.setEnabled(estado);

        Component[] components = panel.getComponents();

        for(int i = 0; i < components.length; i++) {
            if(components[i].getClass().getName() == "javax.swing.JPanel") {
                setHabilitado((JPanel) components[i], estado);
            }
            components[i].setEnabled(estado);
        }
    }
    
}
