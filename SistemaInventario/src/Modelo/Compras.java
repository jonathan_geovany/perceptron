package Modelo;
// Generated 06-18-2016 02:16:13 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Compras generated by hbm2java
 */
public class Compras  implements java.io.Serializable {


     private Integer idCompras;
     private Proveedores proveedores;
     private Usuarios usuarios;
     private String correlativo;
     private Date fecha;
     private String condicionPago;
     private String estatus;
     private Double iva;
     private Double ivaPercibido;
     private Double ivaRetenido;
     private Double subTotal;
     private Double total;
     private Set detallesComprases = new HashSet(0);

    public Compras() {
    }

	
    public Compras(Proveedores proveedores, Usuarios usuarios) {
        this.proveedores = proveedores;
        this.usuarios = usuarios;
    }
    public Compras(Proveedores proveedores, Usuarios usuarios, String correlativo, Date fecha, String condicionPago, String estatus, Double iva, Double ivaPercibido, Double ivaRetenido, Double subTotal, Double total, Set detallesComprases) {
       this.proveedores = proveedores;
       this.usuarios = usuarios;
       this.correlativo = correlativo;
       this.fecha = fecha;
       this.condicionPago = condicionPago;
       this.estatus = estatus;
       this.iva = iva;
       this.ivaPercibido = ivaPercibido;
       this.ivaRetenido = ivaRetenido;
       this.subTotal = subTotal;
       this.total = total;
       this.detallesComprases = detallesComprases;
    }
   
    public Integer getIdCompras() {
        return this.idCompras;
    }
    
    public void setIdCompras(Integer idCompras) {
        this.idCompras = idCompras;
    }
    public Proveedores getProveedores() {
        return this.proveedores;
    }
    
    public void setProveedores(Proveedores proveedores) {
        this.proveedores = proveedores;
    }
    public Usuarios getUsuarios() {
        return this.usuarios;
    }
    
    public void setUsuarios(Usuarios usuarios) {
        this.usuarios = usuarios;
    }
    public String getCorrelativo() {
        return this.correlativo;
    }
    
    public void setCorrelativo(String correlativo) {
        this.correlativo = correlativo;
    }
    public Date getFecha() {
        return this.fecha;
    }
    
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    public String getCondicionPago() {
        return this.condicionPago;
    }
    
    public void setCondicionPago(String condicionPago) {
        this.condicionPago = condicionPago;
    }
    public String getEstatus() {
        return this.estatus;
    }
    
    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }
    public Double getIva() {
        return this.iva;
    }
    
    public void setIva(Double iva) {
        this.iva = iva;
    }
    public Double getIvaPercibido() {
        return this.ivaPercibido;
    }
    
    public void setIvaPercibido(Double ivaPercibido) {
        this.ivaPercibido = ivaPercibido;
    }
    public Double getIvaRetenido() {
        return this.ivaRetenido;
    }
    
    public void setIvaRetenido(Double ivaRetenido) {
        this.ivaRetenido = ivaRetenido;
    }
    public Double getSubTotal() {
        return this.subTotal;
    }
    
    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }
    public Double getTotal() {
        return this.total;
    }
    
    public void setTotal(Double total) {
        this.total = total;
    }
    public Set getDetallesComprases() {
        return this.detallesComprases;
    }
    
    public void setDetallesComprases(Set detallesComprases) {
        this.detallesComprases = detallesComprases;
    }




}


