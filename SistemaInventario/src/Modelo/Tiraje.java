package Modelo;
// Generated 06-18-2016 02:16:13 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Tiraje generated by hbm2java
 */
public class Tiraje  implements java.io.Serializable {


     private Integer idTiraje;
     private Integer numIni;
     private Integer numFin;
     private Integer correlativo;
     private String serie;
     private String estado;
     private String tipo;
     private Set ventases = new HashSet(0);

    public Tiraje() {
    }

    public Tiraje(Integer numIni, Integer numFin, Integer correlativo, String serie, String estado, String tipo, Set ventases) {
       this.numIni = numIni;
       this.numFin = numFin;
       this.correlativo = correlativo;
       this.serie = serie;
       this.estado = estado;
       this.tipo = tipo;
       this.ventases = ventases;
    }
   
    public Integer getIdTiraje() {
        return this.idTiraje;
    }
    
    public void setIdTiraje(Integer idTiraje) {
        this.idTiraje = idTiraje;
    }
    public Integer getNumIni() {
        return this.numIni;
    }
    
    public void setNumIni(Integer numIni) {
        this.numIni = numIni;
    }
    public Integer getNumFin() {
        return this.numFin;
    }
    
    public void setNumFin(Integer numFin) {
        this.numFin = numFin;
    }
    public Integer getCorrelativo() {
        return this.correlativo;
    }
    
    public void setCorrelativo(Integer correlativo) {
        this.correlativo = correlativo;
    }
    public String getSerie() {
        return this.serie;
    }
    
    public void setSerie(String serie) {
        this.serie = serie;
    }
    public String getEstado() {
        return this.estado;
    }
    
    public void setEstado(String estado) {
        this.estado = estado;
    }
    public String getTipo() {
        return this.tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public Set getVentases() {
        return this.ventases;
    }
    
    public void setVentases(Set ventases) {
        this.ventases = ventases;
    }




}


